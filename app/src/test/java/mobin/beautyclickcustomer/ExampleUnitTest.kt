package mobin.beautyclickcustomer

import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun sortedCorrect() {


        val data = sortedAscii(mutableListOf("Relentless Commitment to Impact", "Robust as Fudge", "Simprints, every person counts"
                , "Be Surprisingly Bold", "Get Back Up", "Make it Happen", "Don't be a Jerk", "Confront the Grey", "Laugh Together, Grow Together"))
        assertEquals(data, listOf("Relentless Commitment to Impact", "Simprints, every person counts", "Laugh Together, Grow Together", "Be Surprisingly Bold", "Confront the Grey", "Robust as Fudge", "Make it Happen", "Don't be a Jerk", "Get Back Up"))

    }

    @Test
    fun asciiCorrect() {
        assertEquals(2995, sumOfAscii("Simprints, every person counts"))
    }

    private fun sumOfAscii(s: String): Int {
        var sumOfAscii = 0
        val chars = s.toCharArray()
        for (c in chars) {
            sumOfAscii += c.toInt()
        }
        return sumOfAscii
    }


    private fun sortedAscii(list: MutableList<String>): List<String> {

        val asciisSum = mutableListOf<Int>()

        list.distinctBy {
            asciisSum.add(sumOfAscii(it))
        }
        bubbleSort(list, asciisSum)
        println(list)
        println("In a decreasing Order $asciisSum")



        return list
    }

    // decreasing order swap
    private fun bubbleSort(list: MutableList<String>, numbers: MutableList<Int>) {
        for (i in 0 until (numbers.size - 1)) {
            for (currentPosition in 0 until (numbers.size - 1)) {
                if (numbers[currentPosition] < numbers[currentPosition + 1]) {
                    val tmp = list[currentPosition]
                    list[currentPosition] = list[currentPosition + 1]
                    list[currentPosition + 1] = tmp
                    val tmp1 = numbers[currentPosition]
                    numbers[currentPosition] = numbers[currentPosition + 1]
                    numbers[currentPosition + 1] = tmp1
                }
            }
        }
    }
}

