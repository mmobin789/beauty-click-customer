package mobin.beautyclickcustomer.utils

import android.content.Context
import android.net.ConnectivityManager
import android.util.Patterns
import android.widget.Toast
import mobin.beautyclickcustomer.R
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.text.SimpleDateFormat
import java.util.*


object Utils {

    fun formatDateToTime(date: String): Date {
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return sdf.parse(date)
    }

    fun getTime(): String {
        val sdf = SimpleDateFormat("hh:mm:ss a", Locale.getDefault())
        return sdf.format(Date())
    }

    fun getDate(): String {
        val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return sdf.format(Date())
    }


    fun isValidEmail(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun isNetworkConnected(context: Context) {
        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (cm.activeNetworkInfo == null)
            Toast.makeText(context, R.string.no_net, Toast.LENGTH_SHORT).show()

    }

    fun readFileFromAssets(fileName: String, context: Context): String {
        val returnString = StringBuilder()
        var fIn: InputStream? = null
        var isr: InputStreamReader? = null
        var input: BufferedReader? = null
        try {
            fIn = context.resources.assets
                    .open(fileName, Context.MODE_PRIVATE)
            isr = InputStreamReader(fIn)
            input = BufferedReader(isr)
            val line = input.readLine()
            while (line != null) {
                returnString.append(line)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (isr != null)
                    isr.close()
                if (fIn != null)
                    fIn.close()
                if (input != null)
                    input.close()
            } catch (e2: Exception) {
                e2.printStackTrace()
            }

        }
        return returnString.toString()
    }
//    fun getKeyHash(context: Context): String {
//        var keyhash = ""
//
//        try {
//            @SuppressLint("PackageManagerGetSignatures") val info = context.packageManager.getPackageInfo(
//                    context.packageName,
//                    PackageManager.GET_SIGNATURES)
//            for (signature in info.signatures) {
//                val md = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                keyhash = Base64.encodeToString(md.digest(), Base64.DEFAULT)
//                Log.d("KeyHash:", keyhash)
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//
//        return keyhash
//    }
}