package mobin.beautyclickcustomer.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import mobin.beautyclickcustomer.api.Api
import mobin.beautyclickcustomer.models.User


object AppStorage {
    private var prefs: SharedPreferences? = null
    private val gson = Api.gson
    private const val keyUser = "user"
    fun init(context: Context) {
        if (prefs == null)
            prefs = PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setLanguage(lang: Int) = prefs!!.edit().putInt("lang", lang).apply()

    fun getLanguage() = prefs!!.getInt("lang", 2)

    fun saveUser(user: User) {
        val s = gson.toJson(user)
        prefs!!.edit().putString(keyUser, s).apply()
    }

    fun getUser() = gson.fromJson(prefs!!.getString(keyUser, ""), User::class.java)

    fun clearSession() = prefs!!.edit().clear().apply()

    fun isLoggedIn() = prefs!!.getString(keyUser, "").isNotBlank()

    fun setFilter(filterID: Int) = prefs!!.edit().putInt("filter", filterID).apply()

    fun getFilter() = prefs!!.getInt("filter", 1)

    fun getFCM() = prefs!!.getString("fcm", "")

    fun setFCM(token: String) = prefs!!.edit().putString("fcm", token).apply()

    fun setPublicIPv4(ip: String) = prefs!!.edit().putString("ip", ip).apply()

    fun getPublicIpv4(): String = prefs!!.getString("ip", "")


//    private fun getIpv4(): String? {
//        try {
//            val en = NetworkInterface
//                    .getNetworkInterfaces()
//            while (en.hasMoreElements()) {
//                val intf = en.nextElement()
//                val enumIpAddr = intf
//                        .inetAddresses
//                while (enumIpAddr.hasMoreElements()) {
//                    val inetAddress = enumIpAddr.nextElement()
//
//                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
//                        val ipAddress = inetAddress.getHostAddress().toString()
//                        Log.i("Ipv4", ipAddress)
//                        return ipAddress
//                    }
//                }
//            }
//        } catch (ex: Exception) {
//            Log.e("IP Address", ex.toString())
//        }
//        return null
//    }
}