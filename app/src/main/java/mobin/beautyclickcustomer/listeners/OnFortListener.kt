package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.FortResponse

interface OnFortListener : OnAPIFailListener {
    fun onFortResponse(fortResponse: FortResponse)
}