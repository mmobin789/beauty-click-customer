package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.ApiResponse

interface OnAPIListener : OnAPIFailListener {
    fun onApiResponse(apiResponse: ApiResponse)
}