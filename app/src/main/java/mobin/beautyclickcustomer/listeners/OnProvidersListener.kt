package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.Freelancer
import mobin.beautyclickcustomer.models.Store

interface OnProvidersListener : OnAPIFailListener {
    fun onProvider(store: Store?, freelancer: Freelancer?)
}