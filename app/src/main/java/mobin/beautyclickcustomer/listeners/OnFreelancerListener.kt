package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.Freelancer

interface OnFreelancerListener : OnAPIFailListener {
    fun onFreelancers(freelancer: Freelancer?)
}