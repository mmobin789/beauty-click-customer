package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.ApiResponse

interface OnLoginListener : OnAPIFailListener {
    fun onLogin(loginResponse: ApiResponse)
}