package mobin.beautyclickcustomer.listeners

interface OnIpv4Listener {
    fun onIPv4(ip: String)
}