package mobin.beautyclickcustomer.listeners

interface OnInstagramLoginListener {
    fun onInstagramLogin(accessToken: String)
}