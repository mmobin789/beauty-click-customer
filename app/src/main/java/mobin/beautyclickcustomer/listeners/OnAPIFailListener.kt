package mobin.beautyclickcustomer.listeners

interface OnAPIFailListener {
    fun onApiError(error: String)
}