package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.Store

interface OnStoreListener : OnAPIFailListener {
    fun onStore(store: Store?)
}