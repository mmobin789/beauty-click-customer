package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.adapters.ViewHolder


interface OnListItemClickListener {
    fun onListItemClicked(vh: ViewHolder)
}