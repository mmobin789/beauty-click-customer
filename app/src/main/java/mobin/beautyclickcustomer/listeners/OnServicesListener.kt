package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.ProviderService

interface OnServicesListener : OnAPIFailListener {
    fun onServicesListed(services: ProviderService?)
}