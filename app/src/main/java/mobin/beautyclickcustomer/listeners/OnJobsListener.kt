package mobin.beautyclickcustomer.listeners

import mobin.beautyclickcustomer.models.Job

interface OnJobsListener : OnAPIFailListener {
    fun onJobsListed(jobs: List<Job>?)
}