package mobin.beautyclickcustomer.listeners

interface OnServiceClickListener : OnListItemClickListener {
    fun onServiceUnSelected(position: Int)
}