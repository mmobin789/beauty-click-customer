package mobin.beautyclickcustomer.listeners

interface OnServiceUpdateListener : OnAPIFailListener {
    fun onServiceUpdated(isUpdated: Boolean)
}