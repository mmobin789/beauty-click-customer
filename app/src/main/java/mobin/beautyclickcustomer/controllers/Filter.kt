package mobin.beautyclickcustomer.controllers

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.Window
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.provider_details_dialog.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnAPIListener
import mobin.beautyclickcustomer.models.ApiResponse
import mobin.beautyclickcustomer.models.Provider
import mobin.beautyclickcustomer.utils.AppStorage
import mobin.beautyclickcustomer.utils.AppStorage.setFilter
import java.util.concurrent.TimeUnit

class Filter : Base(), OnAPIListener {

    private lateinit var googleMap: GoogleMap
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var disposable: Disposable? = null

    override fun onApiError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun onApiResponse(apiResponse: ApiResponse) {
        val result = if (apiResponse.status)
            R.string.success
        else R.string.failed
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_filter)
        init()
        setToolbarTitle(getString(R.string.filter))


    }


    private fun searchProvider(s: String) {
        beautyClickImpl.searchProvider(this, s, object : OnAPIListener {
            override fun onApiResponse(apiResponse: ApiResponse) {
                if (apiResponse.status) {
                    updateUI(apiResponse.providers)
                } else Toast.makeText(this@Filter, R.string.not_found, Toast.LENGTH_SHORT).show()
            }

            override fun onApiError(error: String) {
                Toast.makeText(this@Filter, error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateUI(providers: List<Provider>) {
        providers.filterIndexed { index, it ->
            val markerOptions = MarkerOptions()
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher))
            markerOptions.position(LatLng(it.lat, it.lng))
            markerOptions.title(it.provider)
            val marker = googleMap.addMarker(markerOptions)
            marker.tag = index
            zoomToMyLocation(googleMap, it.lat, it.lng)
            true
        }

        googleMap.setOnMarkerClickListener {
            val index = it.tag as Int
            val provider = providers[index]
            providerDetailsDialog(provider)
            true
        }
    }

    private fun providerDetailsDialog(provider: Provider) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.provider_details_dialog)
        dialog.cancelBtn.setOnClickListener {
            dialog.dismiss()
        }
        dialog.typeTV.text = provider.providerType
        dialog.etName.setText(provider.provider)
        dialog.etEmail.setText(provider.email)
        dialog.etPhone.setText(provider.mobile)
        dialog.show()

    }

    private fun rxSearch() {
        val subject = PublishSubject.create<String>()
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                subject.onNext(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }
        })
        disposable = subject.debounce(2, TimeUnit.SECONDS)
                .filter {
                    it.isNotBlank() && it.length > 2
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {


                    googleMap.clear()
                    searchProvider(it)
                }
    }


    private fun init() {
        drawerIV.setImageResource(R.drawable.back)
        filter.visibility = View.GONE
        drawerIV.setOnClickListener {
            onBackPressed()
        }
        val map = map as SupportMapFragment
        map.getMapAsync {
            googleMap = it
            setMapOptions(it)

        }

        rg.setOnCheckedChangeListener { _, checkedId ->
            searchUI.visibility = View.GONE
            when (checkedId) {
                search.id -> searchUI.visibility = View.VISIBLE
                rbNear.id -> setFilter(1)
                rbRank.id -> setFilter(3)
                rbFast.id -> setFilter(2)
                rbRev.id -> setFilter(4)
            }


        }

        val filter = AppStorage.getFilter()

        when (filter) {
            1 -> rbNear.isChecked = true
            2 -> rbFast.isChecked = true
            3 -> rbRank.isChecked = true
            else -> rbRev.isChecked = true
        }

        rxSearch()
        getUserLocation()
    }

//    private fun filterNearest() {
//        if (lat != null && lng != null) {
//            beautyClickImpl.filterNearest(this, LatLng(lat!!, lng!!), this)
//        } else Toast.makeText(this, R.string.turnOnLocation, Toast.LENGTH_SHORT).show()
//    }

    //   private fun filterRanking() = beautyClickImpl.filterRanking(this, this)
    //   private fun filterFastest() = beautyClickImpl.filterFastest(this, this)
    //   private fun filterReviews() = beautyClickImpl.filterReviews(this, this)

    @SuppressLint("MissingPermission")
    private fun getUserLocation() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        val locationRequest = LocationRequest.create()
        locationRequest.interval = 20000
        locationRequest.fastestInterval = 10000
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationProviderClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private val locationCallback = object : LocationCallback() {

        override fun onLocationResult(p0: LocationResult?) {
            if (p0 != null) {
                lat = p0.lastLocation.latitude
                lng = p0.lastLocation.longitude
            }
        }

    }

    override fun onDestroy() {
        if (disposable != null) {
            disposable!!.dispose()
        }
        fusedLocationProviderClient.removeLocationUpdates(locationCallback)
        super.onDestroy()

    }


    companion object {
        private var lat: Double? = null
        private var lng: Double? = null
    }
}
