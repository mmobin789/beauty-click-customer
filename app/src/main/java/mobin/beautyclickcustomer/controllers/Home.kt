package mobin.beautyclickcustomer.controllers

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnServiceUpdateListener
import mobin.beautyclickcustomer.subcontrollers.*
import mobin.beautyclickcustomer.utils.AppStorage.getUser
import mobin.beautyclickcustomer.utils.LanguageController

class Home : Base(), OnServiceUpdateListener {
    private lateinit var baseUI: BaseUI
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        drawer()
    }


    private fun drawer() {
        setToolbarTitle(getString(R.string.home))
        drawerIV.setOnClickListener {
            if (drawerL.isDrawerOpen(Gravity.START))
                drawerL.closeDrawer(Gravity.START, true)
            else drawerL.openDrawer(Gravity.START, true)
        }
        val user = getUser()
        val ui = design_navigation_view.getHeaderView(0) as LinearLayout
        val name = ui.getChildAt(0) as TextView
        val userTV = ui.getChildAt(1) as TextView
        name.text = user.name
        userTV.text = user.username
        design_navigation_view.setNavigationItemSelectedListener {
            val header: String
            baseUI =
                    when (it.itemId) {
                        R.id.nav_completed_job -> {
                            header = getString(R.string.history)
                            JobHistory.newInstance()
                        }
                        R.id.nav_settings -> {
                            header = getString(R.string.settings)
                            Settings.newInstance()

                        }
                        R.id.nav_scheduled -> {
                            header = getString(R.string.scJobs)
                            MyJobs.newInstance()

                        }
                        R.id.nav_profile -> {
                            header = getString(R.string.acc)
                            Profile.newInstance()
                        }
                        R.id.nav_about -> {
                            header = getString(R.string.about)
                            AboutUs.newInstance()
                        }
                        R.id.nav_promo -> {
                            //chooseCategoryDialog()
                            header = getString(R.string.promo)
                            Promotions.newInstance(-1, "")
                        }
                        R.id.nav_phone -> {
                            val call = Intent(Intent.ACTION_DIAL, Uri.parse("tel:920026110"))
                            startActivity(call)
                            header = getString(R.string.home)
                            HomeMenu.newInstance()

                        }
                        else -> {
                            header = getString(R.string.home)
                            HomeMenu.newInstance()
                        }
                    }
            drawerL.closeDrawer(Gravity.START)
            setToolbarTitle(header)
            setFragment(baseUI)
            true
        }
        filter.setOnClickListener {
            startActivity(Intent(it.context, Filter::class.java))
        }


        baseUI = HomeMenu.newInstance()
        setFragment(baseUI)
        beautyClickImpl.fcmToken(this, this)
    }

//    private fun chooseCategoryDialog() {
//        val dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.categories)
//        dialog.drawerIV.visibility = View.INVISIBLE
//        dialog.filter.visibility = View.INVISIBLE
//        dialog.header.setText(R.string.c_cat)
//
//        dialog.barberUI.setOnClickListener {
//            showProviderPromotions(1, "Barber") // barber
//            dialog.dismiss()
//        }
//        dialog.massageUI.setOnClickListener {
//            showProviderPromotions(3, "Massage") // massage
//            dialog.dismiss()
//        }
//        dialog.bathUI.setOnClickListener {
//            showProviderPromotions(4, "Bath") // bath
//            dialog.dismiss()
//        }
//        dialog.salonUI.setOnClickListener {
//            showProviderPromotions(2, "Salon")  // salon
//            dialog.dismiss()
//        }
//
//        dialog.show()
//    }
//
//    private fun showProviderPromotions(catID: Int, categoryName: String) {
//
////        val promoNewJobs = Intent(this, NewJob::class.java)
////        promoNewJobs.putExtra("promo", true)
////        promoNewJobs.putExtra("catID", catID)
////        promoNewJobs.putExtra("service", categoryName)
////        startActivity(promoNewJobs)
//        setToolbarTitle(getString(R.string.promo))
//        setBaseFragment(Promotions.newInstance(catID, categoryName))
//
//
//    }

    override fun onServiceUpdated(isUpdated: Boolean) {
        Log.i("FcmAPI", isUpdated.toString())
    }

    override fun onApiError(error: String) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LanguageController.onAttach(newBase!!))
    }

    private fun setBaseFragment(baseUI: BaseUI) {
        this.baseUI = baseUI
        setFragment(this.baseUI)
    }

    override fun onBackPressed() {
        if (baseUI is HomeMenu)
            super.onBackPressed()
        else {
            setToolbarTitle(getString(R.string.home))
            setBaseFragment(HomeMenu.newInstance())
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (baseUI is MyJobs && requestCode == 5)
            baseUI.onActivityResult(requestCode, resultCode, data)
    }


}
