package mobin.beautyclickcustomer.controllers

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.Window
import android.widget.Toast
import kotlinx.android.synthetic.main.user_agreement.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnLoginListener
import mobin.beautyclickcustomer.models.ApiResponse
import mobin.beautyclickcustomer.models.User
import mobin.beautyclickcustomer.subcontrollers.SignUpForm
import mobin.beautyclickcustomer.utils.AppStorage

class Registration : Base(), OnLoginListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        //   pager.adapter = SignUpPager(supportFragmentManager)
        setFragment(SignUpForm.newInstance())
    }


    //    fun setPageNo(pageNo: Int) {
//        pager.currentItem = pageNo
//    }
    fun userAgreement(user: User) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.user_agreement)
        dialog.agreeTV.movementMethod = ScrollingMovementMethod.getInstance()
        dialog.agreeCB.setOnCheckedChangeListener { _, isChecked ->
            dialog.ok.isEnabled = isChecked


        }
        dialog.ok.setOnClickListener {
            dialog.dismiss()
            showProgressBar()
            beautyClickImpl.signUP(this, user, this)

        }

//        val ua = if (lang == 1) {
//            "user_agreement"
//        } else {
//            "user_agreement_ar"
//        }
//        val uas = Utils.readFileFromAssets(ua, this)
//        dialog.agreeTV.text = uas
        dialog.show()
    }


    override fun onLogin(loginResponse: ApiResponse) {
        dismissProgressBar()
        val status = if (loginResponse.status) {
            AppStorage.saveUser(loginResponse.user)
            startActivity(Intent(this@Registration, Home::class.java))
            onBackPressed()
            getString(R.string.success)
        } else {
            loginResponse.message
        }
        Toast.makeText(this@Registration, status, Toast.LENGTH_SHORT).show()
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        Toast.makeText(this@Registration, error, Toast.LENGTH_SHORT).show()
    }


}
