package mobin.beautyclickcustomer.controllers

import android.os.Bundle
import android.view.View
import android.widget.Toast
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnServiceUpdateListener

class ChangePassword : Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)

        init()
    }

    private fun init() {
        setToolbarTitle(getString(R.string.change_password))
        drawerIV.visibility = View.GONE
        filter.visibility = View.INVISIBLE
        update.setOnClickListener {
            val p = etPassword.text.toString()
            val np = etNewPass.text.toString()
            val cp = etCPassword.text.toString()
            Observable.just(p.isNotBlank() && np.isNotBlank() && cp.isNotBlank() && cp == np)
                    .subscribe {
                        if (it) {
                            showProgressBar()
                            beautyClickImpl.changePassword(this, p, object : OnServiceUpdateListener {
                                override fun onServiceUpdated(isUpdated: Boolean) {
                                    dismissProgressBar()
                                    val s = if (isUpdated)
                                        R.string.success
                                    else R.string.failed
                                    Toast.makeText(this@ChangePassword, s, Toast.LENGTH_SHORT).show()
                                }

                                override fun onApiError(error: String) {
                                    Toast.makeText(this@ChangePassword, error, Toast.LENGTH_SHORT).show()
                                    dismissProgressBar()
                                }
                            })
                        } else
                            Toast.makeText(this@ChangePassword, R.string.input, Toast.LENGTH_SHORT).show()
                    }
        }

    }
}
