package mobin.beautyclickcustomer.controllers

import android.app.Dialog
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.activity_new_job.*
import kotlinx.android.synthetic.main.promocode_dialog.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.ProvidersAdapter
import mobin.beautyclickcustomer.listeners.OnAPIListener
import mobin.beautyclickcustomer.listeners.OnProvidersListener
import mobin.beautyclickcustomer.models.ApiResponse
import mobin.beautyclickcustomer.models.Freelancer
import mobin.beautyclickcustomer.models.Store


class NewJob : Base() {


    //    private var fusedLocationProviderClient: FusedLocationProviderClient? = null

    private var discountPercentage = -1.0f


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_job)

        init()
    }


    private fun init() {
        filter.visibility = View.GONE
        drawerIV.visibility = View.GONE
        val promo = intent.getBooleanExtra("promo", false)
        val header = if (promo)
            getString(R.string.promo)
        else getString(R.string.book)
        setToolbarTitle(header)

        //      if (hasLocationPermission(this))
        //        getUserLocation()
        setAdapter()

    }


    private fun setAdapter() {

        val catID = intent.getIntExtra("catID", -1)
        val adapter = ProvidersAdapter(this, catID)
        pager.adapter = adapter
        tabL.setupWithViewPager(pager)
        setTabBorderUI()


        beautyClickImpl.getProviders(this, catID, object : OnProvidersListener {
            override fun onProvider(store: Store?, freelancer: Freelancer?) {
                adapter.getItem(0).update(store)
                adapter.getItem(1).update(freelancer)
            }

            override fun onApiError(error: String) {
                Toast.makeText(this@NewJob, error, Toast.LENGTH_SHORT).show()
            }
        })


    }

//    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
//        if (requestCode == 3 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
//            getUserLocation()
//        else Toast.makeText(this, "Location Permission is Required", Toast.LENGTH_SHORT).show()
//    }

    fun promoDialog(pid: String) {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.promocode_dialog)
        dialog.ok.setOnClickListener {
            val code = dialog.etCode.text.toString()
            if (code.isNotBlank()) {
                showProgressBar()

                beautyClickImpl.availDiscount(this, pid, code, object : OnAPIListener {
                    override fun onApiResponse(apiResponse: ApiResponse) {
                        dismissProgressBar()
                        Toast.makeText(it.context, if (apiResponse.status) R.string.success else R.string.failed, Toast.LENGTH_SHORT).show()
                        if (apiResponse.status) {
                            discountPercentage = apiResponse.discount
                            dialog.dismiss()
                        }
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        Toast.makeText(it.context, R.string.not_found, Toast.LENGTH_SHORT).show()

                    }
                })
            } else Toast.makeText(it.context, R.string.input, Toast.LENGTH_SHORT).show()
        }

        dialog.show()
    }

    fun newBooking(pid: String, catID: Int, isFreelancer: Boolean, storeLat: Double, storeLng: Double) {
//        val dialog = Dialog(this)
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//        dialog.setContentView(R.layout.add_new_entry_time)
//        val manager = LinearLayoutManager(this)
//        manager.orientation = LinearLayoutManager.HORIZONTAL
//        dialog.rvServices.layoutManager = manager
        val booking = Intent(this, Booking::class.java)
        booking.putExtra("isFreelancer", isFreelancer)
        booking.putExtra("catID", catID)
        booking.putExtra("pid", pid)
        booking.putExtra("discountPercentage", discountPercentage)
        booking.putExtra("storeLatLng", LatLng(storeLat, storeLng))
        startActivity(booking)
    }
//        val servicesSelected = mutableListOf<String>()
//        var bill = 0f
//        if (isFreelancer) {
//            dialog.spinnerUI.visibility = View.GONE
//            dialog.noteTimeSlot.visibility = View.GONE
//            addressTVNewEntry!!.visibility = View.VISIBLE
//            dialog.locBtns.visibility = View.GONE
//            dialog.selectLocTV.setOnClickListener {
//                placePicker()
//            }
//        } else {
//            dialog.locBtns.visibility = View.VISIBLE
//            dialog.locBtns.setOnCheckedChangeListener { _, checkedId ->
//                if (checkedId == dialog.home.id) {
//                    lat = null
//                    lng = null
//                    placePicker()
//                } else {
//                    if (storeLat == null || storeLng == null) {
//                        Toast.makeText(this, R.string.address_na, Toast.LENGTH_SHORT).show()
//                    } else {
//                        lat = storeLat
//                        lng = storeLng
//                    }
//                }
//            }
//            dialog.store.isChecked = true
//        }
//
//        Log.i("cat_pid", catID.toString() + " and " + pid)
//        beautyClickImpl.getProviderServices(this, catID, pid, object : OnServicesListener {
//            override fun onServicesListed(services: ProviderService?) {
//
//                if (services?.services != null && services.services.isNotEmpty()) {
//                    dialog.rvServices.visibility = View.VISIBLE
//                    dialog.rvServices.setItemViewCacheSize(services.services.size)
//                    val adapter = ProviderServicesAdapter(this@NewJob, services.services)
//                    adapter.onServiceClickListener = object : OnServiceClickListener {
//                        override fun onListItemClicked(vh: ViewHolder) {
//                            val data = services.services[vh.adapterPosition]
//                            val service = data.id
//                            bill += getPromotionPrice(data.price)
//                            servicesSelected.add(service)
//                        }
//
//                        override fun onServiceUnSelected(position: Int) {
//                            val data = services.services[position]
//                            val service = data.id
//                            bill -= getPromotionPrice(data.price)
//                            servicesSelected.remove(service)
//                        }
//                    }
//                    dialog.rvServices.adapter = adapter
//
//
//                } else {
//                    dialog.rvServices.visibility = View.GONE
//                    dialog.errorTV.visibility = View.VISIBLE
//                    dialog.errorTV.setText(R.string.not_found)
//                }
//            }
//
//            override fun onApiError(error: String) {
//                dialog.rvServices.visibility = View.GONE
//                dialog.errorTV.visibility = View.VISIBLE
//                dialog.errorTV.text = error
//            }
//        })
//
//        var paymentMethod = "cash"
//        dialog.payBtns.setOnCheckedChangeListener { _, checkedId ->
//            if (checkedId == dialog.cash.id) {
//                paymentMethod = "cash"
//            } else {
//                paymentMethod = "cc"
//
//            }
//        }
//
//        dialog.bookBtn.setOnClickListener {
//            if (servicesSelected.isEmpty())
//                Toast.makeText(it.context, R.string.no_service, Toast.LENGTH_SHORT).show()
//            else {
//                val services = servicesSelected.joinToString("_")
//                Log.i("servicesSelected", services)
//
//                val timeSlot = if (isFreelancer) {
//                    "0"
//
//                } else {
//                    val spinnerTo = dialog.spinnerAMTo.selectedItem.toString()
//                    val spinnerFrom = dialog.spinnerAMFrom.selectedItem.toString()
//                    val hrFrom = dialog.spinnerHRFrom.selectedItem.toString()
//                    val minFrom = dialog.spinnerMinutesFrom.selectedItem.toString()
//                    val hrTo = dialog.spinnerHRTo.selectedItem.toString()
//                    val minTo = dialog.spinnerMinutesTo.selectedItem.toString()
//                    val from = "$hrFrom:$minFrom $spinnerFrom"
//                    val to = "$hrTo:$minTo $spinnerTo"
//
//                    //val timeFrom = Utils.formatDateToTime(from)
//                    //val timeTo = Utils.formatDateToTime(to)
//
//                    // book store
//                    "$from ${getString(R.string.to)} $to"
//                }
//                if (lat == null || lng == null)
//                    Toast.makeText(dialog.context, R.string.address, Toast.LENGTH_SHORT).show()
//                else {
//                    if (paymentMethod == "cc")
//                        makeFortTokenApiCall(services, bill, paymentMethod, dialog, pid, timeSlot)
//                    else bookProvider(paymentMethod, dialog, pid, timeSlot, services)
//                }
//
//
//            }
//        }
//
//        dialog.show()
//
//    }

//    private fun makeCardPayment() {
//        startActivityForResult(Intent(this, Payment::class.java), 5)
//    }


    private fun setTabBorderUI() {
        val l = tabL.getChildAt(0) as LinearLayout
        l.showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
        val g = GradientDrawable()
        g.setColor(ContextCompat.getColor(this, R.color.white))
        g.setSize(resources.getDimensionPixelOffset(R.dimen._1sdp), resources.getDimensionPixelOffset(R.dimen._1sdp))
        l.dividerDrawable = g

    }


//    @SuppressLint("MissingPermission")
//    private fun getUserLocation() {
//        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
//        val locationRequest = LocationRequest.create()
//        locationRequest.interval = 20000
//        locationRequest.fastestInterval = 10000
//        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
//        fusedLocationProviderClient!!.requestLocationUpdates(locationRequest, locationCallback, null)
//    }


//    private val locationCallback = object : LocationCallback() {
//
//        override fun onLocationResult(p0: LocationResult?) {
//            if (p0 != null) {
//                lat = p0.lastLocation.latitude
//                lng = p0.lastLocation.longitude
//            }
//        }
//
//    }

    //    override fun onDestroy() {
//        if (fusedLocationProviderClient != null)
//            fusedLocationProviderClient!!.removeLocationUpdates(locationCallback)
//        super.onDestroy()
//
//    }

//    private fun getIpv4(): String? {
//        try {
//            val en = NetworkInterface
//                    .getNetworkInterfaces()
//            while (en.hasMoreElements()) {
//                val intf = en.nextElement()
//                val enumIpAddr = intf
//                        .inetAddresses
//                while (enumIpAddr.hasMoreElements()) {
//                    val inetAddress = enumIpAddr.nextElement()
//
//                    if (!inetAddress.isLoopbackAddress && inetAddress is Inet4Address) {
//                        val ipaddress = inetAddress.getHostAddress().toString()
//                        return ipaddress
//                    }
//                }
//            }
//        } catch (ex: Exception) {
//            Log.e("IP Address", ex.toString())
//        }
//        return null
//    }


}
