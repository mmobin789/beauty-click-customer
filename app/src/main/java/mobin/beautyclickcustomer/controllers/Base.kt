package mobin.beautyclickcustomer.controllers

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent
import android.view.Window
import android.widget.ImageView
import android.widget.RelativeLayout
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.wang.avi.AVLoadingIndicatorView
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.models.GlideApp
import mobin.beautyclickcustomer.subcontrollers.BaseUI
import mobin.beautyclickcustomer.utils.AppStorage
import mobin.beautyclickcustomer.utils.LanguageController
import mobin.beautyclickcustomer.utils.Utils
import mobin.beautyclickcustomer.viewmodels.BeautyClickImpl
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.util.*

abstract class Base : AppCompatActivity() {

//    fun pickImage(iPickResult: IPickResult) {
//        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(this).apply {
//            setOnPickCancel { dismiss() }
//        }
//    }

    protected fun setFragment(baseUI: BaseUI) {
        supportFragmentManager.beginTransaction().replace(R.id.container, baseUI).commit()
    }

    fun setToolbarTitle(header: String) {

        this.header.text = header
    }

    override fun recreate() {
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun attachBaseContext(newBase: Context?) {

        super.attachBaseContext(CalligraphyContextWrapper.wrap(LanguageController.onAttach(newBase!!)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        progressBar = createProgressBar(this)
        beautyClickImpl = ViewModelProviders.of(this).get(BeautyClickImpl::class.java)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).build())
        Utils.isNetworkConnected(this)
        AppStorage.init(this)
    }

    companion object {
        lateinit var beautyClickImpl: BeautyClickImpl
        var lang = 1
        //      var fcmData: Bundle? = null
        fun loadWithGlide(path: String, iv: ImageView, isCircle: Boolean) {
            val request = GlideApp.with(iv.context).load(path).centerCrop().placeholder(R.drawable.logo)
            if (isCircle)
                request.apply(RequestOptions.circleCropTransform())
            request.into(iv)
        }

        fun loadWithGlide(bitmap: Bitmap, iv: ImageView) {
            GlideApp.with(iv).load(bitmap).placeholder(R.drawable.logo).into(iv)
        }

        private fun updateUI(context: Context, language: String) {
            LanguageController.setLocale(context, language)


        }

        fun switchLocale(context: Context): Int {
            val lang = Locale.getDefault().language
            val langI = if (lang.equals("en", ignoreCase = true)) {

                updateUI(context, "ar")
                2
            } else {
                updateUI(context, "en")
                1
            }
            AppStorage.setLanguage(langI)
            return langI
        }

        fun drawPolyLines(googleMap: GoogleMap, from: LatLng, to: LatLng) {
            // val markerOptions = MarkerOptions()   / code for adding marker to polyline as well
            //     markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.Hue))
            //   markerOptions.position(from)
            //   val markerOptions2 = MarkerOptions()
            //   markerOptions2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
            //  markerOptions2.position(to)
            //  googleMap.addMarker(markerOptions)
            //  googleMap.addMarker(markerOptions2)
            val line = googleMap.addPolyline(PolylineOptions()
                    .add(from, to))
            line.color = Color.BLACK
            line.width = 12f
            line.isGeodesic = true
            val cap = BitmapDescriptorFactory.fromResource(R.drawable.line_cap)
            line.startCap = CustomCap(cap)
            line.endCap = CustomCap(cap)
            line.jointType = JointType.ROUND
            val pattern = listOf(Gap(20f), Dash(20f))
            line.pattern = pattern
//        val builder = LatLngBounds.Builder()
//        builder.include(to)
//        builder.include(from)

            try {
                val bounds = LatLngBounds(from, to)
                googleMap.setLatLngBoundsForCameraTarget(bounds)
                googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 32))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun hasLocationPermission(base: Base): Boolean {
            var permission = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permission = ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(base, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (!permission) {

                    base.requestPermissions(arrayOf(
                            Manifest
                                    .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                    ), 3)
                }
            }
            return permission
        }

        @SuppressLint("MissingPermission")
        fun setMapOptions(googleMap: GoogleMap) {
            googleMap.isMyLocationEnabled = true
            googleMap.uiSettings.setAllGesturesEnabled(true)
        }

        fun zoomToMyLocation(googleMap: GoogleMap, lat: Double, lng: Double) {

            val latLng = LatLng(lat, lng)
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 12f)
            googleMap.animateCamera(cameraUpdate)

        }

        fun showProgressBar() {
            try {
                progressBar.show()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        fun dismissProgressBar() {

            progressBar.dismiss()

        }

        private lateinit var progressBar: Dialog
        private fun createProgressBar(context: Context): Dialog {

            val dialog = Dialog(context)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setOnKeyListener { _, _, p2 ->
                if (p2.keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()

                }
                true
            }
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val relativeLayout = RelativeLayout(context)
            val avLoadingIndicatorView = AVLoadingIndicatorView(context)
            avLoadingIndicatorView.setIndicatorColor(ContextCompat.getColor(context, R.color.colorPrimaryDark))
            //avLoadingIndicatorView.setIndicator(BallPulseIndicator())
            val params = RelativeLayout.LayoutParams(150, 150)
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
            avLoadingIndicatorView.layoutParams = params
            relativeLayout.addView(avLoadingIndicatorView)
            dialog.setContentView(relativeLayout)
            return dialog


        }


    }


}