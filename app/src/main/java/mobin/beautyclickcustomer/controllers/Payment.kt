package mobin.beautyclickcustomer.controllers

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.common.hash.Hashing
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.utils.AppStorage
import mobin.beautyclickcustomer.utils.Utils
import okhttp3.MediaType
import okhttp3.RequestBody
import java.nio.charset.Charset

class Payment : Base() {

    private val merchantID = "MqCpffOv"
    private val accessCode = "sZfsS4LvfWRdJMwVw45U"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)

        init()
    }

    private fun init() {
        setToolbarTitle("Payment")
        filter.visibility = View.INVISIBLE
        etName.setText(AppStorage.getUser().name)
        ok.setOnClickListener {

            if (validatedForm())
            else Toast.makeText(it.context, R.string.input, Toast.LENGTH_SHORT).show()

        }

    }

    private fun validatedForm() =
            etCard.text.isNotBlank() && etCvv.text.isNotBlank() && etM.text.isNotBlank() && etY.text.isNotBlank()

    private fun getReferenceStamp(): String {
        val date = Utils.getDate()
        return "BC-${System.currentTimeMillis() / 1000L}-($date)"

    }

    private fun getLanguage() = if (AppStorage.getLanguage() == 2) "en" else "ar"
    private fun createSDKSignature(): String {
        val s = ("TESTSHAINaccess_code=${accessCode}" +
                "language=${getLanguage()}merchant_identifier=${merchantID}merchant_reference=${getReferenceStamp()}service_command=TOKENIZATIONTESTSHAIN")
        val signS = Hashing.sha256().hashString(s, Charset.defaultCharset()).toString()
        Log.i("SortedForSignature", s)
        Log.i("Signature", signS)
        return signS

    }

    private fun getTextRequestBody(data: String): RequestBody {
        return RequestBody.create(MediaType.parse("text/plain"), data)
    }

    private fun getSdkTokenParams(): HashMap<String, RequestBody> {
        val params = hashMapOf<String, RequestBody>()
        val customer = etName.text.toString()
        val cardNo = etCard.text.toString()

        val cvv = etCvv.text.toString()
        val expiryMonth = etM.text.toString()
        val expiryYear = etY.text.toString()
        params["service_command"] = getTextRequestBody("TOKENIZATION")
        params["access_code"] = getTextRequestBody(accessCode)
        params["merchant_identifier"] = getTextRequestBody(merchantID)
        params["language"] = getTextRequestBody(getLanguage())
        params["signature"] = getTextRequestBody(createSDKSignature())
        params["expiry_date"] = getTextRequestBody(expiryMonth + expiryYear)
        params["card_number"] = getTextRequestBody(cardNo)
        params["card_security_code"] = getTextRequestBody(cvv)
        if (customer.isNotBlank())
            params["card_holder_name"] = getTextRequestBody(customer)
        return params
    }
//
//    private fun makeSDKTokenRequest() {
//        showProgressBar()
//        beautyClickImpl.getFortToken(this, getSdkTokenParams(), object : OnFortListener {
//            override fun onFortResponse(fortResponse: FortResponse) {
//                dismissProgressBar()
//
//            }
//
//            override fun onApiError(error: String) {
//                dismissProgressBar()
//            }
//        })
//    }
}
