package mobin.beautyclickcustomer.controllers

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.Toast
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.twitter.sdk.android.core.*
import com.twitter.sdk.android.core.identity.TwitterAuthClient
import com.twitter.sdk.android.core.models.User
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.Observables
import kotlinx.android.synthetic.main.activity_login.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnInstagramLoginListener
import mobin.beautyclickcustomer.listeners.OnLoginListener
import mobin.beautyclickcustomer.models.ApiResponse
import mobin.beautyclickcustomer.models.Instagram
import mobin.beautyclickcustomer.utils.AppStorage
import java.util.*


class Login : Base(), OnInstagramLoginListener, OnLoginListener {

    private var callbackManager: CallbackManager? = null
    private var client: TwitterAuthClient? = null
    private var loginType = Login.facebook
    private var disposable: Disposable? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()


    }

    private fun setLanguage(position: Int) {
        val systemLang = Locale.getDefault().language
        if (position == 1 && systemLang != "en") {
            lang = switchLocale(this@Login)
            recreate()
        } else if (position == 2 && systemLang != "ar") {
            lang = switchLocale(this@Login)
            recreate()
        }
    }

    private fun init() {
        autoLogin()
        spinnerLanguage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                setLanguage(position)
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
        signUp.setOnClickListener {
            startActivity(Intent(it.context, Registration::class.java))
        }
        login.setOnClickListener {

            validateThenLogin()
        }

        loginTwitter.setOnClickListener {
            twitterLogin(it)
        }
        loginFB.setOnClickListener {
            fbLogin()
        }
        hasLocationPermission(this)


    }

    fun loginInsta(v: View) {
        Instagram.loginInstagram(v.context, this)
    }

    private fun autoLogin() {
        if (AppStorage.isLoggedIn()) {
            onBackPressed()
            homeScreen()
        }
    }

    private fun homeScreen() {
        startActivity(Intent(this, Home::class.java))
    }

    private fun validateThenLogin() {
        val name = etName.text.toString()
        val password = etPassword.text.toString()
        val nameObservable =
                Observable.just(name)
                        .map {
                            it.isNotBlank()
                        }
        val passwordObservable = Observable.just(password)
                .map {
                    it.isNotBlank()
                }

        val result: Observable<Boolean> = Observables.combineLatest(nameObservable, passwordObservable) { n, p -> n && p }
        disposable = result.subscribe {
            if (it) {
                showProgressBar()
                beautyClickImpl.doLogin(this, name, password, lang, this)
            } else
                Toast.makeText(this, R.string.input, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onInstagramLogin(accessToken: String) {

        beautyClickImpl.getInstaUser(accessToken).observe(this, Observer {
            val instaUser = it!!.data
            Log.i("Instagram", instaUser.username)
            val signUp = Intent(this, Registration::class.java)
            signUp.putExtra("instaUser", it.data)
            startActivity(signUp)
        })
    }

    override fun onLogin(loginResponse: ApiResponse) {
        dismissProgressBar()
        if (loginResponse.status) {
            disposable!!.dispose()
            AppStorage.saveUser(loginResponse.user)
            homeScreen()
        } else
            Toast.makeText(this@Login, R.string.una, Toast.LENGTH_SHORT).show()
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show()
    }

    private fun getFacebookProfile(loginResult: LoginResult) {
        val graphRequest = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, _ ->
            Log.i("FacebookGraphAPI", `object`.toString())

            val signUp = Intent(this, Registration::class.java)
            val picture = Profile.getCurrentProfile().getProfilePictureUri(200, 200)
            signUp.putExtra("name", `object`.getString("name"))
            signUp.putExtra("picture", picture.toString())
            signUp.putExtra("email", `object`.getString("email"))
            signUp.putExtra("fbUser", true)
            startActivity(signUp)
        }
        val params = Bundle()
        params.putString("fields", "name,email,picture")
        graphRequest.parameters = params
        graphRequest.executeAsync()
    }


    private fun twitterLogin(v: View) {
        loginType = Login.twitter
        Twitter.initialize(v.context)
        client = TwitterAuthClient()
        client!!.authorize(this, object : Callback<TwitterSession>() {
            override fun success(result: Result<TwitterSession>) {

                //  getTwitterEmail(result)

                val user = TwitterCore.getInstance().getApiClient(result.data).accountService.verifyCredentials(false, false, true)
                user.enqueue(object : Callback<User>() {
                    override fun success(resultUser: Result<User>) {
                        val signUp = Intent(this@Login, Registration::class.java)
                        signUp.putExtra("username", result.data.userName)
                        signUp.putExtra("name", resultUser.data.name)
                        signUp.putExtra("picture", resultUser.data.profileImageUrl)
                        signUp.putExtra("email", resultUser.data.email)
                        signUp.putExtra("twitterUser", true)
                        startActivity(signUp)
                    }

                    override fun failure(exception: TwitterException) {
                        Log.e("Twitter", exception.toString())
                    }
                })

            }

            override fun failure(exception: TwitterException) {
                Log.e("Twitter", exception.toString())
            }
        })
    }

    private fun fbLogin() {
        callbackManager = CallbackManager.Factory.create()
        LoginManager.getInstance().apply {
            logInWithReadPermissions(this@Login, listOf("email", "public_profile"))
            registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onError(error: FacebookException?) {
                    Log.e("Facebook", error.toString())
                }

                override fun onSuccess(result: LoginResult) {
                    getFacebookProfile(result)
                }

                override fun onCancel() {

                }

            })
        }

    }

    enum class Login {
        twitter, facebook
    }

    private fun getTwitterEmail(login: Result<TwitterSession>) {

        client!!.requestEmail(login.data, object : Callback<String>() {
            override fun success(result: Result<String>) {
                // Do something with the result, which provides the email address
                val email = result.data
                Log.i("Twitter", email + " ${login.data.userName}")
            }

            override fun failure(exception: TwitterException) {
                // Do something on failure
                Log.e("TwitterEmail", exception.toString())
            }
        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (loginType) {
            Login.facebook -> {
                callbackManager!!.onActivityResult(requestCode, resultCode, data)
            }
            else -> {
                client!!.onActivityResult(requestCode, resultCode, data)
            }
        }


    }

}
