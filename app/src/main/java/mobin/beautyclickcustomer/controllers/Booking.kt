package mobin.beautyclickcustomer.controllers

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.places.ui.PlacePicker
import com.google.android.gms.maps.model.LatLng
import com.google.common.hash.Hashing
import com.payfort.fort.android.sdk.base.FortSdk
import com.payfort.fort.android.sdk.base.callbacks.FortCallBackManager
import com.payfort.sdk.android.dependancies.base.FortInterfaces
import com.payfort.sdk.android.dependancies.models.FortRequest
import kotlinx.android.synthetic.main.activity_booking.*
import kotlinx.android.synthetic.main.toolbar.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.ProviderServicesAdapter
import mobin.beautyclickcustomer.adapters.ViewHolder
import mobin.beautyclickcustomer.listeners.*
import mobin.beautyclickcustomer.models.FortResponse
import mobin.beautyclickcustomer.models.ProviderService
import mobin.beautyclickcustomer.utils.AppStorage
import java.nio.charset.Charset
import java.util.*
import kotlin.math.roundToInt

class Booking : Base() {

    private var lat: Double? = null
    private var lng: Double? = null
    private var fortToken = ""
    private var fortCallBackManager: FortCallBackManager? = null
    private val merchantID = "MqCpffOv"
    private val accessCode = "sZfsS4LvfWRdJMwVw45U"
    private var quantity = 1
    private var bill = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking)
        init()
    }


    private fun init() {
        setToolbarTitle(getString(R.string.book))
        filter.visibility = View.INVISIBLE
        drawerIV.visibility = View.GONE
        rvServices.layoutManager = LinearLayoutManager(this)
        val discountPercentage = intent.getFloatExtra("discountPercentage", -1.0f)
        val catID = intent.getIntExtra("catID", -1)
        val pid = intent.getStringExtra("pid")
        val isFreelancer = intent.getBooleanExtra("isFreelancer", false)
        val latLng = intent.getParcelableExtra<LatLng>("storeLatLng")
        val promotionMode = intent.getBooleanExtra("promo", false)
        if (isFreelancer) {
            spinnerUI.visibility = View.GONE
            noteTimeSlot.visibility = View.GONE
            selectLocTV.visibility = View.VISIBLE
            locBtns.visibility = View.GONE
            selectLocTV.setOnClickListener {
                placePicker()
            }
        } else {
            locBtns.visibility = View.VISIBLE
            locBtns.setOnCheckedChangeListener { _, checkedId ->
                if (checkedId == home.id) {
                    lat = null
                    lng = null
                    placePicker()
                } else {
                    if (latLng.latitude == -1.0 || latLng.longitude == -1.0) {
                        Toast.makeText(this, R.string.address_na, Toast.LENGTH_SHORT).show()
                        lat = null
                        lng = null
                    } else {
                        lat = latLng.latitude
                        lng = latLng.longitude
                    }
                }
            }
            store.isChecked = true
        }
        fortCallBackManager = FortCallBackManager.Factory.create()
        val servicesSelected = mutableListOf<String>()
        Log.i("cat_pid", catID.toString() + " and " + pid)

        if (promotionMode) {
            rvServices.visibility = View.VISIBLE
            val services = intent.getParcelableArrayListExtra<ProviderService.Data>("promotions")
            rvServices.setItemViewCacheSize(services.size)
            val adapter = ProviderServicesAdapter(services, discountPercentage)
            adapter.onServiceClickListener = object : OnServiceClickListener {
                override fun onListItemClicked(vh: ViewHolder) {
                    val data = services[vh.adapterPosition]
                    val service = data.id
                    bill += data.price
                    servicesSelected.add(service)
                    updateAmount()

                }

                override fun onServiceUnSelected(position: Int) {
                    val data = services[position]
                    val service = data.id
                    bill -= data.price
                    servicesSelected.remove(service)
                    updateAmount()
                }
            }
            rvServices.adapter = adapter
        } else {
            quantityUI.visibility = View.GONE
            beautyClickImpl.getProviderServices(this, catID, pid, object : OnServicesListener {
                override fun onServicesListed(services: ProviderService?) {

                    if (services?.services != null && services.services.isNotEmpty()) {
                        quantityUI.visibility = View.VISIBLE
                        rvServices.visibility = View.VISIBLE
                        rvServices.setItemViewCacheSize(services.services.size)
                        val adapter = ProviderServicesAdapter(services.services, discountPercentage)
                        adapter.onServiceClickListener = object : OnServiceClickListener {
                            override fun onListItemClicked(vh: ViewHolder) {
                                val data = services.services[vh.adapterPosition]
                                val service = data.id
                                bill += data.price
                                servicesSelected.add(service)
                                updateAmount()
                            }

                            override fun onServiceUnSelected(position: Int) {
                                val data = services.services[position]
                                val service = data.id
                                bill -= data.price
                                servicesSelected.remove(service)
                                updateAmount()
                            }
                        }
                        rvServices.adapter = adapter


                    } else {
                        rvServices.visibility = View.GONE
                        errorTV.visibility = View.VISIBLE
                        errorTV.setText(R.string.not_found)
                    }
                }

                override fun onApiError(error: String) {
                    rvServices.visibility = View.GONE
                    errorTV.visibility = View.VISIBLE
                    errorTV.text = error
                }
            })
        }

        var paymentMethod = "cash"

        cash.setOnClickListener {
            paymentMethod = "cash"
            cc.isChecked = false
        }

        cc.setOnClickListener {
            paymentMethod = "cc"
            cash.isChecked = false
        }

        bookBtn.setOnClickListener {
            if (servicesSelected.isEmpty())
                Toast.makeText(it.context, R.string.no_service, Toast.LENGTH_SHORT).show()
            else {
                val services = servicesSelected.joinToString("_")
                Log.i("servicesSelected", services)

                val timeSlot = if (isFreelancer) {
                    "0"

                } else {
                    val spinnerTo = spinnerAMTo.selectedItem.toString()
                    val spinnerFrom = spinnerAMFrom.selectedItem.toString()
                    val hrFrom = spinnerHRFrom.selectedItem.toString()
                    val minFrom = spinnerMinutesFrom.selectedItem.toString()
                    val hrTo = spinnerHRTo.selectedItem.toString()
                    val minTo = spinnerMinutesTo.selectedItem.toString()
                    val from = "$hrFrom:$minFrom $spinnerFrom"
                    val to = "$hrTo:$minTo $spinnerTo"

                    //val timeFrom = Utils.formatDateToTime(from)
                    //val timeTo = Utils.formatDateToTime(to)

                    // book store
                    "$from ${getString(R.string.to)} $to"
                }
                if (lat == null || lng == null)
                    Toast.makeText(this, R.string.address, Toast.LENGTH_SHORT).show()
                else {
                    if (paymentMethod == "cc")
                        makeFortTokenApiCall(services, bill, paymentMethod, pid, timeSlot)
                    else bookProvider(paymentMethod, pid, timeSlot, services)
                }


            }
        }

        setQuantityView()
        getIpString()

    }

    private fun setQuantityView() {

        updateAmount()

        etQuantity.setText(quantity.toString())

        etQuantity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {

            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                quantity = if (count > 0 && s.toString().toInt() != 0) {
                    s.toString().toInt()
                } else {
                    1
                }
                updateAmount()
            }
        })
    }

    private fun updateAmount() {
        val formattedAmount = String.format("%.2f", bill * quantity)
        val data = "${getString(R.string.total_amount)} $formattedAmount ${getString(R.string.sar)}"
        amountTV.text = data
        amountTV.visibility = if (bill <= 0)
            View.GONE
        else View.VISIBLE
    }

    private fun getIpString() {
        if (AppStorage.getPublicIpv4().isBlank()) {
            beautyClickImpl.getPublicIP(this, object : OnIpv4Listener {
                override fun onIPv4(ip: String) {
                    AppStorage.setPublicIPv4(ip)
                }
            })
        } else return
    }

    private fun makeFortTokenApiCall(services: String, bill: Float, paymentMethod: String, pid: String, timeSlot: String) {
        showProgressBar()
        beautyClickImpl.getFortToken(this, getSdkTokenParams(), object : OnFortListener {
            override fun onFortResponse(fortResponse: FortResponse) {
                dismissProgressBar()
                if (!fortResponse.sdkToken.isNullOrBlank()) {
                    fortToken = fortResponse.sdkToken
                    makeFortRequest(getPurchaseRequestParams(services, bill), services, paymentMethod, pid, timeSlot)
                } else Toast.makeText(this@Booking, R.string.failed, Toast.LENGTH_SHORT).show()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(this@Booking, error, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun getSdkTokenParams(): HashMap<String, String> {
        val params = hashMapOf<String, String>()
        params["service_command"] = "SDK_TOKEN"
        params["access_code"] = accessCode
        params["merchant_identifier"] = merchantID
        params["language"] = getLanguage()
        params["device_id"] = getDeviceID()
        params["signature"] = createSDKTokenSignature()
        return params
    }

    private fun placePicker() {
        val placePickIntent = PlacePicker.IntentBuilder()
        try {
            startActivityForResult(placePickIntent.build(this), 3)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, e.localizedMessage, Toast.LENGTH_SHORT).show()
        }


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 3 && resultCode == Activity.RESULT_OK) {
            val place = PlacePicker.getPlace(this, data)
            lat = place.latLng.latitude
            lng = place.latLng.longitude
            selectLocTV.visibility = View.VISIBLE
            selectLocTV.text = place.address

        } else if (requestCode == 5 && resultCode == Activity.RESULT_OK) {
            fortCallBackManager!!.onActivityResult(requestCode, resultCode, data)
        }
    }


    private fun bookProvider(paymentMethod: String, pid: String, timeSlot: String, services: String) {

        showProgressBar()
        beautyClickImpl.bookProvider(this, pid, timeSlot, services, LatLng(lat!!, lng!!), paymentMethod, object : OnServiceUpdateListener {
            override fun onServiceUpdated(isUpdated: Boolean) {
                dismissProgressBar()
                val result = if (isUpdated) {
                    onBackPressed()
                    R.string.thanks
                } else R.string.failed

                Toast.makeText(this@Booking, result, Toast.LENGTH_SHORT).show()

            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                Toast.makeText(this@Booking, error, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun getPurchaseRequestParams(services: String, bill: Float): FortRequest {
        val fortRequest = getFortRequest()
        val user = AppStorage.getUser()
        val requestMap = hashMapOf<String, Any>()
        requestMap["command"] = "PURCHASE"
        requestMap["customer_email"] = user.email.replace("_", "@") // tested
        requestMap["currency"] = "SAR"
        requestMap["amount"] = ((bill * 100).roundToInt() * quantity) // tested
        requestMap["language"] = getLanguage()
        requestMap["merchant_reference"] = getOrderStamp()
        requestMap["customer_name"] = user.name // tested
        val ip = AppStorage.getPublicIpv4()
        Log.i("Ipv4", ip)
        requestMap["customer_ip"] = ip
        //  requestMap["phone_number"] = user.phone
        // requestMap.put("payment_option”, "VISA”);
        // requestMap.put("eci”, "ECOMMERCE”);
        requestMap["order_description"] = services
        requestMap["sdk_token"] = fortToken
        fortRequest.requestMap = requestMap
        return fortRequest
    }


    private fun getLanguage() = if (AppStorage.getLanguage() == 2) "en" else "ar"

    private fun createSDKTokenSignature(): String {
        val s = "TESTSHAINaccess_code=$accessCode" +
                "device_id=${getDeviceID()}language=${getLanguage()}merchant_identifier=${merchantID}service_command=SDK_TOKENTESTSHAIN"

        val signS = Hashing.sha256().hashString(s, Charset.defaultCharset()).toString()
        Log.i("SortedForSignature", s)
        Log.i("Signature", signS)
        return signS

    }


    private fun getDeviceID(): String {
        val deviceID = FortSdk.getDeviceId(this)
        Log.i("Fort Device ID", deviceID)
        return deviceID
    }

    private fun getFortRequest(): FortRequest {
        val fortRequest = FortRequest()
        fortRequest.isShowResponsePage = true
        return fortRequest
    }


    private fun getOrderStamp() =
    //  val date = Utils.getDate()
    //    val time = Utils.getTime()
            "BC-${System.currentTimeMillis() / 1000L}"

    private fun makeFortRequest(fortRequest: FortRequest, services: String, paymentMethod: String, pid: String, timeSlot: String) {
        FortSdk.getInstance().registerCallback(this, fortRequest, FortSdk.ENVIRONMENT.TEST, 5, fortCallBackManager, true, object : FortInterfaces.OnTnxProcessed {
            override fun onSuccess(p0: MutableMap<String, Any>, p1: MutableMap<String, Any>) {
                //  Toast.makeText(this@Booking, p1["response_message"].toString(), Toast.LENGTH_SHORT).show()
                bookProvider(paymentMethod, pid, timeSlot, services)

            }

            override fun onFailure(p0: MutableMap<String, Any>, p1: MutableMap<String, Any>) {
                Toast.makeText(this@Booking, p1["response_message"].toString(), Toast.LENGTH_SHORT).show()
            }

            override fun onCancel(p0: MutableMap<String, Any>?, p1: MutableMap<String, Any>) {
                Toast.makeText(this@Booking, android.R.string.cancel, Toast.LENGTH_SHORT).show()
            }
        })
    }
}
