package mobin.beautyclickcustomer.api

import io.reactivex.Observable
import mobin.beautyclickcustomer.models.*
import okhttp3.ResponseBody
import retrofit2.http.*

interface WebService {


    @GET("${Instagram.insta_BASE_URL}v1/users/self")
    fun getInstagramUser(@Query("access_token") accessToken: String): Observable<Instagram>

    @GET
    fun api(@Url url: String): Observable<ApiResponse>

    @GET
    fun getProviderServices(@Url url: String): Observable<ProviderService>

    @GET
    fun getJobs(@Url url: String): Observable<List<Job>>

    @GET
    fun getStores(@Url url: String): Observable<Store>


    @GET
    fun getFreelancers(@Url url: String): Observable<Freelancer>

    @POST(Api.fortTestTokenUrl)
    fun getFortSDKToken(@Body params: HashMap<String, String>): Observable<FortResponse>

    @GET("http://checkip.amazonaws.com/")
    fun getPublicIpv4Address(): Observable<ResponseBody>


}