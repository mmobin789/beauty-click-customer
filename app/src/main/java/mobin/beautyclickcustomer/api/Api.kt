package mobin.beautyclickcustomer.api

import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import mobin.beautyclickcustomer.models.User
import mobin.beautyclickcustomer.utils.AppStorage
import mobin.beautyclickcustomer.utils.AppStorage.getFCM
import mobin.beautyclickcustomer.utils.AppStorage.getUser
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object Api {
    private const val baseUrl = "http://www.beautyclickk.com/beautyclick/"
    const val fortTestTokenUrl = "https://sbpaymentservices.payfort.com/FortAPI/paymentApi"
    const val fortProductionTokenUrl = "https://paymentservices.payfort.com/FortAPI/paymentApi"
    private var retrofit: Retrofit? = null
    val gson = Gson()
    private fun getWebApi(): WebService {
        if (retrofit == null) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            val httpClient = OkHttpClient.Builder().addInterceptor(loggingInterceptor).build()
            retrofit = Retrofit.Builder().baseUrl(baseUrl)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    //      .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient)
                    .build()
        }

        return retrofit!!.create(WebService::class.java)
    }


    fun getInstaUser(token: String) =
            getWebApi().getInstagramUser(token)

    fun login(username: String, password: String, lang: Int) = getWebApi().api(baseUrl + "login_customer/$username/$password/$lang")

    fun signUP(user: User) = getWebApi().api(baseUrl + "signup_customer/$user")

    fun getServices(providerID: String, categoryID: Int) =
            getWebApi().getProviderServices(baseUrl + "services_g_c/$providerID/${getUser().getGender()}/$categoryID")

    fun editProfile(user: User) = getWebApi().api(baseUrl + "upd_profile/${user.id}/${user.name}/${user.username}/${user.phone}/0/${user.disabled}")
    fun changePassword(password: String) = getWebApi().api(baseUrl + "upd_password/${getUser().id}/$password")

    fun changeLanguage(lang: Int) = getWebApi().api(baseUrl + "upd_langauge/${getUser().id}/$lang")

    fun getFreelancers(catID: Int) = getWebApi().getFreelancers(baseUrl + "freelacner_services/$catID/${AppStorage.getFilter()}/${getUser().id}")

    fun getStores(catID: Int) = getWebApi().getStores(baseUrl + "stores_service_id/$catID/${AppStorage.getFilter()}/${getUser().id}")

    fun fcmToken() = getWebApi().api(baseUrl + "fcm_customer/${getUser().id}/${getFCM()}")

    fun bookProvider(pid: String, timeSlot: String, services: String, latLng: LatLng, paymentMethod: String) =
            getWebApi().api(baseUrl + "ins_appointment/${getUser().id}/$pid/$timeSlot/$services/${latLng.latitude}/${latLng.longitude}/$paymentMethod")

    fun searchProvider(s: String) = getWebApi().api(baseUrl + "search_provider/$s")

    fun filterByNearest(latLng: LatLng) = getWebApi().api(baseUrl + "filter_nearest/${getUser().id}/${latLng.latitude}/${latLng.longitude}")

    fun filterByFastest() = getWebApi().api(baseUrl + "filter_fastest/${getUser().id}")

    fun filterByRanking() = getWebApi().api(baseUrl + "filter_raking/${getUser().id}")

    fun filterByReviews() = getWebApi().api(baseUrl + "filter_reviews/${getUser().id}")

    fun getJobs() = getWebApi().getJobs(baseUrl + "orders_cid/${getUser().id}")

    fun cancelOrder(orderID: String) = getWebApi().api(baseUrl + "cancel_order/${getUser().id}/$orderID")

    fun completedOrder(orderID: String) = getWebApi().api(baseUrl + "complete_order/${getUser().id}/$orderID")

    fun rateProvider(pid: String, rating: Float, review: String) = getWebApi().api(baseUrl + "rating/${getUser().id}/$pid/$rating/$review")

    fun availDiscount(pid: String, promo: String) = getWebApi().api(baseUrl + "promotion/${getUser().id}/$pid/$promo")

    fun getFortSDKToken(params: HashMap<String, String>) = getWebApi().getFortSDKToken(params)

    fun getPromotions() = getWebApi().api(baseUrl + "get_promotions_c")

    fun getPublicIpv4() = getWebApi().getPublicIpv4Address()
}