package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_freelancer.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.FreelancersAdapter
import mobin.beautyclickcustomer.adapters.ViewHolder
import mobin.beautyclickcustomer.listeners.OnListItemClickListener
import mobin.beautyclickcustomer.models.Freelancer

class ProviderFreelancer : BaseUI() {
    private var catID = 0
    //private var promo = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_freelancer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        catID = arguments!!.getInt("catID")
        // promo = arguments!!.getBoolean("promo")
        rv.layoutManager = LinearLayoutManager(context)
        errorTV.setText(R.string.com_facebook_loading)
        // makeApiCall()


    }

    override fun update(any: Any?) {
        onFreelancers(any as Freelancer?)
    }

//    private fun makeApiCall() {
//
//            beautyClickImpl.getFreelancers(this, catID, this)
//
//
//    }


    private fun onFreelancers(freelancer: Freelancer?) {

        errorTV.visibility = View.GONE

        if (freelancer?.freelancers != null) {
            val list = freelancer.freelancers
            if (list.isNotEmpty()) {
                val adapter = FreelancersAdapter(newJob, false, freelancer.service, list)
                adapter.onListItemClickListener = object : OnListItemClickListener {
                    override fun onListItemClicked(vh: ViewHolder) {
                        val pid = freelancer.freelancers[vh.adapterPosition].id
                        if (pid.isNullOrBlank())
                            Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show()
                        else
                            newJob.newBooking(pid!!, catID, true, -1.0, -1.0)
                    }


                }
                rv.adapter = adapter
            } else showListErrorUI(R.string.not_found)
        } else showListErrorUI(R.string.not_found)
    }

//    override fun onApiError(error: String) {
//
//        onApiListError(error)
//    }

//    override fun onApiListError(error: String) {
//        super.onApiListError(error)
//        showListErrorUI(R.string.not_found)
//    }

//    override fun showListErrorUI(resID: Int) {
//
//        errorTV.setText(resID)
//        errorTV.visibility = View.VISIBLE
//
//    }

//    private fun getPromotionFreelancersIfAvailable(list: List<Freelancer.Data>): List<Freelancer.Data> {
//        val discounted = mutableListOf<Freelancer.Data>()
//
//
//        return if (promo) {
//            list.distinctBy {
//                val promotion = it.promotion
//                if (promotion != null && promotion)
//                    discounted.add(it)
//
//            }
//            discounted
//        } else list
//    }

    companion object {

        //        fun newInstance(catID: Int, promo: Boolean): ProviderFreelancer {
//            val providerFreelancer = ProviderFreelancer()
//            val bundle = Bundle()
//            bundle.putInt("catID", catID)
//            bundle.putBoolean("promo", promo)
//            providerFreelancer.arguments = bundle
//            return providerFreelancer
//
//        }
        private var providerFreelancer: ProviderFreelancer? = null

        fun newInstance(catID: Int): ProviderFreelancer {
            if (providerFreelancer == null)
                providerFreelancer = ProviderFreelancer()
            val bundle = Bundle()
            bundle.putInt("catID", catID)
            providerFreelancer!!.arguments = bundle
            return providerFreelancer!!

        }
    }
}

