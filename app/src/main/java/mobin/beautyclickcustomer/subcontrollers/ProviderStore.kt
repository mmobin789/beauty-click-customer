package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_store.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.StoresAdapter
import mobin.beautyclickcustomer.adapters.ViewHolder
import mobin.beautyclickcustomer.listeners.OnListItemClickListener
import mobin.beautyclickcustomer.models.Store

class ProviderStore : BaseUI() {
    private var catID = 0
    //  private var promo = false
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_store, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        catID = arguments!!.getInt("catID")
        //   promo = arguments!!.getBoolean("promo")
        rv.layoutManager = LinearLayoutManager(context)
        errorTV.setText(R.string.com_facebook_loading)
        retainInstance
        //   makeApiCall()


    }
//
//    private fun makeApiCall() {
//
//        beautyClickImpl.getStores(this, catID, this)
//
//
//    }

    override fun update(any: Any?) {
        onStore(any as Store?)
    }

    private fun onStore(store: Store?) {

        errorTV.visibility = View.GONE

        if (store?.stores != null) {
            val list = store.stores
            if (list.isNotEmpty()) {
                val adapter = StoresAdapter(newJob, false, store.service, list)
                adapter.onListItemClickListener = object : OnListItemClickListener {
                    override fun onListItemClicked(vh: ViewHolder) {
                        val employee = store.stores[vh.adapterPosition]
                        val pid = employee.id
                        if (pid.isNullOrBlank())
                            Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show()
                        else {
                            var lat = employee.lat
                            var lng = employee.lng

                            if (lat == null || lng == null) {
                                lat = -1.0
                                lng = -1.0
                            }
                            newJob.newBooking(pid!!, catID, false, lat, lng)

                        }
                    }


                }
                rv.adapter = adapter
            } else showListErrorUI(R.string.not_found)
        } else showListErrorUI(R.string.not_found)
    }

//    override fun onApiError(error: String) {
//        onApiListError(error)
//    }

//    override fun onApiListError(error: String) {
//        super.onApiListError(error)
//        showListErrorUI(R.string.not_found)
//    }

//    override fun showListErrorUI(resID: Int) {
//
//        errorTV.setText(resID)
//        errorTV.visibility = View.VISIBLE
//
//    }

//    private fun getPromotionStoresIfAvailable(list: List<Store.Employee>): List<Store.Employee> {
//        val discounted = mutableListOf<Store.Employee>()
//
//        return if (promo) {
//            list.distinctBy {
//                val promotion = it.promotion
//                if (promotion != null && promotion)
//                    discounted.add(it)
//            }
//            discounted
//        } else list
//    }

    companion object {
        //        fun newInstance(catID: Int, promo: Boolean): ProviderStore {
//            val providerStore = ProviderStore()
//            val bundle = Bundle()
//            bundle.putInt("catID", catID)
//            bundle.putBoolean("promo", promo)
//            providerStore.arguments = bundle
//            return providerStore
//        }
        private var providerStore: ProviderStore? = null

        fun newInstance(catID: Int): ProviderStore {
            if (providerStore == null)
                providerStore = ProviderStore()
            val bundle = Bundle()
            bundle.putInt("catID", catID)
            providerStore!!.arguments = bundle
            return providerStore!!

        }
    }

}