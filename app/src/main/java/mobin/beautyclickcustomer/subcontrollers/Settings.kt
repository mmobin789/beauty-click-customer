package mobin.beautyclickcustomer.subcontrollers

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.settings.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base
import mobin.beautyclickcustomer.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickcustomer.controllers.Base.Companion.showProgressBar
import mobin.beautyclickcustomer.controllers.ChangePassword
import mobin.beautyclickcustomer.controllers.Login
import mobin.beautyclickcustomer.listeners.OnServiceUpdateListener
import mobin.beautyclickcustomer.utils.AppStorage


class Settings : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.settings, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        lang.setOnClickListener {
            showProgressBar()
            beautyClickImpl.changeLanguage(this, Base.switchLocale(context!!), object : OnServiceUpdateListener {
                override fun onServiceUpdated(isUpdated: Boolean) {
                    val result = if (isUpdated) {
                        R.string.success
                    } else
                        R.string.failed

                    Toast.makeText(context, result, Toast.LENGTH_SHORT).show()

                    if (result == R.string.success)
                        home.recreate()
                }

                override fun onApiError(error: String) {
                    Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
                }
            })
        }

        password.setOnClickListener {
            startActivity(Intent(it.context, ChangePassword::class.java))
        }

        logout.setOnClickListener {
            AppStorage.clearSession()
            home.finish()
            val login = Intent(it.context, Login::class.java)
            login.flags = Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT
            startActivity(login)
        }
    }

    companion object {
        fun newInstance(): Settings = Settings()
    }
}