package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.PromotionsAdapter
import mobin.beautyclickcustomer.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickcustomer.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickcustomer.controllers.Base.Companion.showProgressBar
import mobin.beautyclickcustomer.listeners.OnAPIListener
import mobin.beautyclickcustomer.models.ApiResponse

class Promotions : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        val catID = arguments!!.getInt("catID")
        val categoryName = arguments!!.getString("service")

        getPromotions(categoryName, catID)

    }

    private fun getPromotions(categoryName: String, catID: Int) {
        showProgressBar()
        beautyClickImpl.getPromotions(this, object : OnAPIListener {
            override fun onApiResponse(apiResponse: ApiResponse) {
                val list = apiResponse.promotions
                if (list.isNotEmpty())
                    rv.adapter = PromotionsAdapter(list)
                else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                onApiListError(error)

            }
        })
    }

    override fun onApiListError(error: String) {
        super.onApiListError(error)
        showListErrorUI(R.string.not_found)
    }

    companion object {
        fun newInstance(catID: Int, categoryName: String): Promotions {
            val fragment = Promotions()
            fragment.arguments = Bundle().apply {
                putInt("catID", catID)
                putString("service", categoryName)
            }

            return fragment
        }
    }
}