package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_about_us.*
import mobin.beautyclickcustomer.R

class AboutUs : BaseUI() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_us, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        aboutUsTV.movementMethod = ScrollingMovementMethod.getInstance()
    }

    companion object {
        fun newInstance() = AboutUs()
    }
}