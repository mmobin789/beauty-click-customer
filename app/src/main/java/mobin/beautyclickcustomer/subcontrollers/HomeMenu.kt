package mobin.beautyclickcustomer.subcontrollers

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.NewJob

class HomeMenu : BaseUI(), View.OnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        barberUI.setOnClickListener(this)
        massageUI.setOnClickListener(this)
        salonUI.setOnClickListener(this)
        bathUI.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        val service = when (v.id) {
            barberUI.id -> {
                "Barber"
            }
            massageUI.id -> {
                "Massage"
            }
            bathUI.id -> {
                "Bath"
            }
            else -> {
                "Salon"
            }

        }
        val catID = when (v.id) {
            barberUI.id -> {
                1
            }
            massageUI.id -> {
                3
            }
            bathUI.id -> {
                4
            }
            else -> {
                2
            }

        }

        val newJob = Intent(v.context, NewJob::class.java)
        newJob.putExtra("service", service)
        newJob.putExtra("catID", catID)
        startActivity(newJob)
    }

    companion object {
        fun newInstance() = HomeMenu()
    }


}