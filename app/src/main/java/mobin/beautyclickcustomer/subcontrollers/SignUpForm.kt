package mobin.beautyclickcustomer.subcontrollers

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.PopupMenu
import android.widget.TextView
import android.widget.Toast
import io.reactivex.Observable
import kotlinx.android.synthetic.main.dob_dialog.*
import kotlinx.android.synthetic.main.sign_up_form.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base
import mobin.beautyclickcustomer.models.Instagram
import mobin.beautyclickcustomer.models.User
import java.util.*

class SignUpForm : BaseUI() {
    private val user = User("-1", Base.lang.toString())

    companion object {
        fun newInstance() = SignUpForm()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.sign_up_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        signUp.setOnClickListener {
            user.username = etUsername.text.toString()
            user.name = etName.text.toString()
            user.phone = etPhone.text.toString()
            user.password = etCPassword.text.toString()
            user.email = etEmail.text.toString()


            Observable.just(user.isValid() && etCPassword.text.toString() == etPassword.text.toString())
                    .subscribe {
                        if (it) {
                            registration.userAgreement(user)
                        } else Toast.makeText(context, R.string.input, Toast.LENGTH_SHORT).show()
                    }
        }
//        spinnerDisabled.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
//                user.disabled = position
//            }
//
//
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//
//            }
//        }
        agreeCB.setOnCheckedChangeListener { _, isChecked ->
            user.disabled = if (isChecked) 1 else 0
        }

        spinnerGender.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position > 0) {

                    user.gender = if (position == 1)
                        "male"   //  api code 0 for male and 1 for female
                    else "female"
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        login.setOnClickListener {
            registration.onBackPressed()
        }
        chooseDOB.setOnClickListener {
            takeDOB(it)
        }

        if (registration.intent.hasExtra("fbUser")) {
            etName.setText(registration.intent.getStringExtra("name"))
            etEmail.setText(registration.intent.getStringExtra("email"))
            etName.isEnabled = false
            etEmail.isEnabled = false
        } else if (registration.intent.hasExtra("instaUser")) {
            val user = registration.intent.getParcelableExtra<Instagram.InstagramUser>("instaUser")
            etName.setText(user.fullName)
            etUsername.setText(user.username)
            etUsername.isEnabled = false
            etName.isEnabled = false
        } else if (registration.intent.hasExtra("twitterUser")) {
            etName.setText(registration.intent.getStringExtra("name"))
            etUsername.setText(registration.intent.getStringExtra("username"))
            etEmail.setText(registration.intent.getStringExtra("email"))
            etUsername.isEnabled = false
            etName.isEnabled = false
            etEmail.isEnabled = false
        }

    }

    private fun takeDOB(it: View) {
        val dialog = Dialog(it.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dob_dialog)
        dialog.show()

        dialog.dayTV.setOnClickListener {
            chooseDayMenu(it)
        }
        dialog.monthTV.setOnClickListener {
            chooseMonthMenu(it)
        }

        dialog.yearTV.setOnClickListener {
            chooseYearMenu(it)
        }

        dialog.ok.setOnClickListener {
            val day = dialog.dayTV.text
            val month = dialog.monthTV.text
            val year = dialog.yearTV.text

            Observable.just(day.length <= 2 && month.length <= 2 && year.length >= 4 && year.toString().toIntOrNull() != null).subscribe {
                if (it) {
                    val dob = "$year-$month-$day"
                    user.dob = dob
                    dobTV.text = dob
                    dialog.dismiss()
                } else Toast.makeText(dialog.context, R.string.input, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun chooseDayMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        val daysInCurrentMonth = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH)
        for (i in 1..daysInCurrentMonth) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }

    private fun chooseMonthMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        for (i in 1..12) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }

    private fun chooseYearMenu(it: View) {
        val tv = it as TextView
        val popupMenu = PopupMenu(it.context, it)
        val menu = popupMenu.menu

        popupMenu.setOnMenuItemClickListener {
            tv.text = it.title.toString()
            true
        }
        val year = Calendar.getInstance().get(Calendar.YEAR)
        for (i in (year - 40)..year) {
            menu.add(i.toString())
        }

        popupMenu.show()

    }
}