package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickcustomer.controllers.Home
import mobin.beautyclickcustomer.controllers.NewJob
import mobin.beautyclickcustomer.controllers.Registration

abstract class BaseUI : Fragment() {


    lateinit var registration: Registration
    lateinit var home: Home
    lateinit var newJob: NewJob

//    fun pickImage(iPickResult: IPickResult) {
//        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(fragmentManager).apply {
//            setOnPickCancel { dismiss() }
//        }
//    }

    open fun onApiListError(error: String) {
        dismissProgressBar()
        Toast.makeText(context, R.string.not_found, Toast.LENGTH_SHORT).show()

    }


    open fun showListErrorUI(@StringRes resID: Int) {
        errorTV.setText(resID)
        errorTV.visibility = View.VISIBLE
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (activity is Registration)
            registration = activity as Registration
        else if (activity is Home) home = activity as Home
        else newJob = activity as NewJob
    }

    open fun update(any: Any?) {
        return
    }
}