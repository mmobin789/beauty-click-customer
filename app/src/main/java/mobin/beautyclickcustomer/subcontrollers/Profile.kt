package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.profile.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base
import mobin.beautyclickcustomer.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickcustomer.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickcustomer.controllers.Base.Companion.showProgressBar
import mobin.beautyclickcustomer.listeners.OnServiceUpdateListener
import mobin.beautyclickcustomer.models.User
import mobin.beautyclickcustomer.utils.AppStorage.getUser
import mobin.beautyclickcustomer.utils.Utils

class Profile : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val user = getUser()
        val updateUser = User(user.id, Base.lang.toString())
        etName.setText(user.name)
        etUsername.setText(user.username)
        etEmail.setText(user.email.replace("_", "@"))
        etPhone.setText(user.phone)
        profileEditing(false)

        edit.setOnClickListener {
            profileEditing(true)
        }
        agreeCB.setOnCheckedChangeListener { _, isChecked ->
            updateUser.disabled = if (isChecked) 1 else 0
        }
        save.setOnClickListener {


            updateUser.name = etName.text.toString()
            updateUser.username = etUsername.text.toString()
            updateUser.phone = etPhone.text.toString()
            updateUser.email = etEmail.text.toString()

            if (Utils.isValidEmail(updateUser.email)) {
                profileEditing(false)
                showProgressBar()
                beautyClickImpl.editProfile(this, updateUser, object : OnServiceUpdateListener {
                    override fun onServiceUpdated(isUpdated: Boolean) {
                        if (isUpdated)
                            Toast.makeText(it.context, R.string.success, Toast.LENGTH_SHORT).show()
                        dismissProgressBar()
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        Toast.makeText(it.context, error, Toast.LENGTH_SHORT).show()

                    }
                })
            } else Toast.makeText(it.context, R.string.email, Toast.LENGTH_SHORT).show()
        }

        status.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked)
                statusTV.setText(R.string.online)
            else statusTV.setText(R.string.offline)


        }
    }

    private fun profileEditing(enable: Boolean) {
        etEmail.isEnabled = enable
        etName.isEnabled = enable
        etPhone.isEnabled = enable
        save.isEnabled = enable
    }

    companion object {
        private var profile: Profile? = null
        fun newInstance(): Profile {
            if (profile == null)
                profile = Profile()
            return profile!!
        }
    }
}