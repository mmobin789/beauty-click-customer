package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import kotlinx.android.synthetic.main.choose_lang.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base

class ChooseLanguage : BaseUI() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.choose_lang, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        //spinnerLanguage.adapter = getSpinnerAdapter(resources.getStringArray(R.array.lang))
        // spinnerProvider.adapter = getSpinnerAdapter(resources.getStringArray(R.array.providers))
        next.setOnClickListener {
            //  registration.setPageNo(1)

        }
        spinnerLanguage.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 2 || position == 1) {
                    Base.switchLocale(context!!)
                    registration.recreate()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
        spinnerProvider.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 1)
                    etCode.visibility = View.VISIBLE
                else etCode.visibility = View.GONE
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    companion object {
        fun newInstance() = ChooseLanguage()
    }
}