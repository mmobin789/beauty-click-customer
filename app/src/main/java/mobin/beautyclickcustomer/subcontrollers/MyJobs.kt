package mobin.beautyclickcustomer.subcontrollers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_my_jobs.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.adapters.MyJobsAdapter
import mobin.beautyclickcustomer.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickcustomer.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickcustomer.controllers.Base.Companion.showProgressBar
import mobin.beautyclickcustomer.listeners.OnJobsListener
import mobin.beautyclickcustomer.models.Job

class MyJobs : BaseUI() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        rv.layoutManager = LinearLayoutManager(context)
        showProgressBar()
        beautyClickImpl.getJobs(this, object : OnJobsListener {

            override fun onJobsListed(jobs: List<Job>?) {

                val list = getAcceptedJobs(jobs)
                if (list.isNotEmpty())
                    rv.adapter = MyJobsAdapter(this@MyJobs, list)
                else showListErrorUI(R.string.not_found)
                dismissProgressBar()
            }

            override fun onApiError(error: String) {
                dismissProgressBar()
                onApiListError(error)
                showListErrorUI(R.string.not_found)
            }
        })


    }

    private fun getAcceptedJobs(jobs: List<Job>?): MutableList<Job> {
        val acceptedJobs = mutableListOf<Job>()
        jobs?.distinctBy {
            if (it.status.equals("Accepted", true))
                acceptedJobs.add(it)
        }

        return acceptedJobs
    }



    companion object {
        private var homeMenu: MyJobs? = null
        fun newInstance(): MyJobs {
            if (homeMenu == null)
                homeMenu = MyJobs()
            return homeMenu!!
        }
    }
}