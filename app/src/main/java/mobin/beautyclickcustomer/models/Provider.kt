package mobin.beautyclickcustomer.models

import com.google.gson.annotations.SerializedName

data class Provider
(
        val provider: String, val category: String?, @SerializedName("provider_type") val providerType: String, val lat: Double, val lng: Double
        , val mobile: String, val email: String
)