package mobin.beautyclickcustomer.models

import mobin.beautyclickcustomer.utils.Utils

data class User(val id: String, val language: String) {
    var name = ""
    var username = ""
    var gender = ""
    var email = ""
    var phone = ""
    var dob = ""
    var password = ""
    var disabled = 0
    override fun toString(): String {
        return "$language/$phone/$name/$username/${getGender()}/$dob/$password/${email.replace("@", "_")}/$disabled".replace(" ", "")
    }

    fun isValid() = phone.isNotBlank() && name.isNotBlank() && username.isNotBlank() && gender.isNotBlank() && dob.isNotBlank() && password.isNotBlank()
            && Utils.isValidEmail(email)

    fun getGender() = if (gender.equals("male", true)) 0 else 1

}