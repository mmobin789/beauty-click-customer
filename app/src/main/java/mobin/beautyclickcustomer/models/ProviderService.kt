package mobin.beautyclickcustomer.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class ProviderService(

        val status: Boolean, val pid: Int, val services: List<Data>
) {
    @Parcelize
    data class Data(val service: String,

                    val id: String,

                    val isOffered: Boolean,

                    val price: Float

            //val discount: Float
    ) : Parcelable
}