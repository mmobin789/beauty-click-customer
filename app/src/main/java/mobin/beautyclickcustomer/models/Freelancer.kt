package mobin.beautyclickcustomer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Freelancer(val service: String, @SerializedName("freelancer") val freelancers: List<Freelancer.Data>) : Parcelable {
    @Parcelize
    data class Data(@SerializedName("user_id") val id: String?, val service: String?, val name: String?, @SerializedName("minprice") val minPrice: String?
                    , val discount: String?, val promotion: Boolean?, val mobile: String?, val image: String?) : Parcelable
}