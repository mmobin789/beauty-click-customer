package mobin.beautyclickcustomer.models

data class Promotion(val title: String?, val provider: String, val providerName: String, val providerPhone: String
                     , val image: String, val pid: Int, val id: Int, val services: List<Service>?, val lat: Double?, val lng: Double?) {
    data class Service(val id: Int, val serviceName: String, val price: Float)
}