package mobin.beautyclickcustomer.models

import com.google.gson.annotations.SerializedName


data class Job(
        val jobRating: Float,

        @field:SerializedName("user type")
        val userType: String,


        val jobPostDate: String?,
        @field:SerializedName("user_id")
        val userId: String,

        @field:SerializedName("service")
        val service: String,

        @field:SerializedName("price")
        val price: String,

        @field:SerializedName("service_id")
        val serviceId: String,

        @field:SerializedName("order id")
        val orderId: String,

        @field:SerializedName("customer_id")
        val customerId: String,

        @field:SerializedName("status")
        val status: String,

        @field:SerializedName("username")
        val username: String,

        @field:SerializedName("customer")
        val customer: String,

        val city: String,

        val lat: Double?,

        val lng: Double?
)