package mobin.beautyclickcustomer.models

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Parcelable
import android.util.Log
import android.view.Window
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.auth_dialog_instagram.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnInstagramLoginListener

data class Instagram(
        val data: InstagramUser) {
    @Parcelize
    data class InstagramUser(val id: String, val username: String, @field:SerializedName("full_name") val fullName: String,
                             @field:SerializedName("profile_picture") val profilePic: String,
                             val bio: String
    ) : Parcelable

    companion object {

        @SuppressLint("SetJavaScriptEnabled")
        fun loginInstagram(context: Context, onInstagramLoginListener: OnInstagramLoginListener) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.auth_dialog_instagram)
            dialog.show()
            dialog.webView.settings.javaScriptEnabled = true
            dialog.webView.loadUrl(Instagram.insta_auth_url)

            dialog.webView.webViewClient = object : WebViewClient() {
                private var accessToken = ""

                override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                    Log.e("Instagram Error", error.toString())
                }

                override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                    Log.d("Insta_url", url)
                    if (url.contains("#access_token=")) {
                        val uri = Uri.parse(url)
                        accessToken = uri.encodedFragment
                        accessToken = accessToken.substring(accessToken.lastIndexOf("=") + 1)
                        Log.i("InstagramToken", accessToken)
                        onInstagramLoginListener.onInstagramLogin(accessToken)
                        dialog.dismiss()

                    } else if (url.contains("?error")) {
                        Toast.makeText(context, "Error Occured", Toast.LENGTH_SHORT).show()
                        dialog.dismiss()
                    }
                }
            }
        }


        const val insta_BASE_URL = "https://api.instagram.com/"
        private const val insta_auth_url = "${insta_BASE_URL}oauth/authorize/?client_id=13056a0ca739449eb5691024b1425bf4&redirect_uri=https://www.beautyclickk.com/home&response_type=token&scope=public_content"
    }
}