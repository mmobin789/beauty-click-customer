package mobin.beautyclickcustomer.models

import com.google.gson.annotations.SerializedName

data class ApiResponse(val status: Boolean,
                       val discount: Float,
                       val user: User,
                       val service: String?,
                       @SerializedName("freelancer")
                       val freelancers: List<Freelancer>,
                       val message: String,
                       val providers: List<Provider>,
                       val promotions: List<Promotion>
)

