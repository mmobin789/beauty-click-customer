package mobin.beautyclickcustomer.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Store(
        @SerializedName("Service") val service: String,
        @SerializedName("Stores") val stores: List<Employee>


) : Parcelable {
    @Parcelize
    data class Employee(@SerializedName("user_id") val id: String?, val name: String, val profession: String, @SerializedName("minprice") val minPrice: String, val dailyTimings: List<String>?, val image: String?, val lat: Double?, val lng: Double?) : Parcelable


}

