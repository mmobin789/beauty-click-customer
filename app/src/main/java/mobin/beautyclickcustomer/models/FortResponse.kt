package mobin.beautyclickcustomer.models

import com.google.gson.annotations.SerializedName

data class FortResponse(
        @SerializedName("service_command")
        val serviceCommand: String,
        @SerializedName("access_code")
        val accessCode: String,
        @SerializedName("merchant_identifier")
        val merchantID: String,
        val token: String,
        val language: String,
        @SerializedName("device_id")
        val deviceID: String,
        val signature: String,
        @SerializedName("sdk_token")
        val sdkToken: String?,
        @SerializedName("response_message")
        val message: String,
        @SerializedName("response_code")
        val code: Int,
        val status: Int

)