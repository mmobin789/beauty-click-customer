package mobin.beautyclickcustomer.models

data class WorkingHours(val days: List<String>, val timeSlot: List<String>)