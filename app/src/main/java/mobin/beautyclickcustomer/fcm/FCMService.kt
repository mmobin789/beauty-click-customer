package mobin.beautyclickcustomer.fcm

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import mobin.beautyclickcustomer.utils.AppStorage

class FCMService : FirebaseMessagingService() {
    override fun onNewToken(p0: String?) {
        Log.d(javaClass.simpleName, "Refreshed token: " + p0!!)
        AppStorage.setFCM(p0)
    }
}
