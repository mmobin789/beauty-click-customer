package mobin.beautyclickcustomer.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.google.android.gms.maps.model.LatLng
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.Schedulers.io
import mobin.beautyclickcustomer.api.Api
import mobin.beautyclickcustomer.controllers.Base
import mobin.beautyclickcustomer.listeners.*
import mobin.beautyclickcustomer.models.*
import mobin.beautyclickcustomer.subcontrollers.BaseUI
import okhttp3.ResponseBody

class BeautyClickImpl : ViewModel() {
    private lateinit var disposable: Disposable

    fun getInstaUser(token: String): MutableLiveData<Instagram> {
        val instaUser = MutableLiveData<Instagram>()
        disposable = Api.getInstaUser(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    Log.i("InstagramUserAPI", Api.gson.toJson(it))
                    instaUser.postValue(it)
                },
                {
                    Log.e("InstagramUserAPI", it.toString())
                })

        return instaUser
    }

    fun signUP(base: Base, user: User, onLoginListener: OnLoginListener) {

        disposable = Api.signUP(user).observeOn(mainThread())
                .subscribeOn(io()).subscribe({
                    val apiResponse = MutableLiveData<ApiResponse>()
                    apiResponse.postValue(it)
                    apiResponse.observe(base, Observer {
                        Log.i("SignUpAPI", Api.gson.toJson(it))
                        onLoginListener.onLogin(it!!)
                        disposable.dispose()
                    })
                }, {
                    onLoginListener.onApiError(it.toString())
                    Log.e("SignUpAPI", it.toString())
                })
    }

    fun doLogin(base: Base, username: String, password: String, lang: Int, onLoginListener: OnLoginListener) {
        val user = MutableLiveData<ApiResponse>()
        disposable = Api.login(username, password, lang).subscribeOn(io()).observeOn(mainThread()).subscribe(
                {
                    Log.i("LoginAPI", Api.gson.toJson(it))
                    user.postValue(it)
                    user.observe(base, Observer {

                        onLoginListener.onLogin(it!!)
                        disposable.dispose()

                    })
                }
                , {
            Log.e("LoginAPI", it.toString())
        }
        )
    }

    fun editProfile(baseUI: BaseUI, user: User, onServiceUpdateListener: OnServiceUpdateListener) {
        disposable = Api.editProfile(user).observeOn(mainThread()).subscribeOn(Schedulers.io())
                .subscribe({
                    val data = MutableLiveData<ApiResponse>()
                    data.postValue(it)
                    data.observe(baseUI, Observer {
                        onServiceUpdateListener.onServiceUpdated(it!!.status)
                        disposable.dispose()
                    })
                }, {
                    Log.e("ProfileAPI", it.toString())
                })
    }

    fun getProviderServices(base: Base, categoryID: Int, providerID: String, onServicesListener: OnServicesListener) {
        val services = MutableLiveData<ProviderService>()
        disposable = Api.getServices(providerID, categoryID).subscribeOn(io())
                .observeOn(mainThread())
                .subscribe({
                    val jsonString = Api.gson.toJson(it)
                    Log.i("ProviderServicesAPI", jsonString)
                    services.postValue(it)
                    services.observe(base, Observer {
                        onServicesListener.onServicesListed(it)
                    })

                }, {
                    Log.e("ProviderServicesAPI", it.toString())
                    onServicesListener.onApiError(it.toString())
                }
                )


    }

    fun changePassword(base: Base, password: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.changePassword(password).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("ChangePasswordAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("ChangePasswordAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )

    }

    fun changeLanguage(baseUI: BaseUI, lang: Int, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.changeLanguage(lang).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("ChangeLanguageAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("ChangeLanguageAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

//    fun getFreelancers(baseUI: BaseUI, catID: Int, onFreelancerListener: OnFreelancerListener) {
//        val data = MutableLiveData<Freelancer>()
//        disposable = Api.getFreelancers(catID).subscribeOn(Schedulers.io())
//                .observeOn(mainThread()).subscribe(
//                        {
//                            data.postValue(it)
//                            data.observe(baseUI, Observer {
//                                Log.i("FreelancersAPI", Api.gson.toJson(it))
//                                onFreelancerListener.onFreelancers(it)
//                                disposable.dispose()
//                            })
//                        }, {
//                    Log.e("FreelancersAPI", it.toString())
//                    onFreelancerListener.onApiError(it.toString())
//                }
//                )
//    }

//    fun getStores(baseUI: BaseUI, catID: Int, onStoreListener: OnStoreListener) {
//        val data = MutableLiveData<Store>()
//        disposable = Api.getStores(catID).subscribeOn(Schedulers.io())
//                .observeOn(mainThread()).subscribe(
//                        {
//                            data.postValue(it)
//                            data.observe(baseUI, Observer {
//                                Log.i("StoresAPI", Api.gson.toJson(it))
//                                onStoreListener.onStore(it)
//                                disposable.dispose()
//                            })
//                        }, {
//                    Log.e("StoresAPI", it.toString())
//                    onStoreListener.onApiError(it.toString())
//                }
//                )
//    }

    fun getProviders(base: Base, catID: Int, onProvidersListener: OnProvidersListener) {
        var employees: Store? = null
        disposable = Api.getStores(catID).flatMap {

            val data = MutableLiveData<Store>()
            data.postValue(it)
            data.observe(base, Observer { store ->
                employees = store
            })

            Api.getFreelancers(catID)
        }.subscribeOn(Schedulers.io()).observeOn(mainThread()).subscribe(
                {
                    val data = MutableLiveData<Freelancer>()
                    data.postValue(it)
                    data.observe(base, Observer { freelancers ->
                        onProvidersListener.onProvider(employees, freelancers)
                        disposable.dispose()
                    })
                }, {
            it.printStackTrace()
            onProvidersListener.onApiError(it.toString())
        }, {
            Log.i("getProviders", "Success")
        }
        )
    }

    fun fcmToken(base: Base, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.fcmToken().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer { data ->
                                Log.i("FcmAPI", Api.gson.toJson(data))
                                onServiceUpdateListener.onServiceUpdated(data!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FcmAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun bookProvider(base: Base, pid: String, timeSlot: String, services: String, latLng: LatLng, paymentMethod: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.bookProvider(pid, timeSlot, services, latLng, paymentMethod).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("BookProviderAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("BookProviderAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun searchProvider(base: Base, s: String, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.searchProvider(s).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("SearchProviderAPI", Api.gson.toJson(it))
                                onAPIListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("SearchProviderAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }

    fun filterNearest(base: Base, latLng: LatLng, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.filterByNearest(latLng).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("FilterNearestAPI", Api.gson.toJson(it))
                                onAPIListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FilterNearestAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }

    fun filterRanking(base: Base, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.filterByRanking().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("FilterRankingAPI", Api.gson.toJson(it))
                                onAPIListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FilterRankingAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }

    fun filterReviews(base: Base, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.filterByReviews().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("FilterRankingAPI", Api.gson.toJson(it))
                                onAPIListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FilterRankingAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }


    fun filterFastest(base: Base, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.filterByFastest().subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer {
                                Log.i("FilterRankingAPI", Api.gson.toJson(it))
                                onAPIListener.onApiResponse(it!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("FilterRankingAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }

    fun getJobs(baseUI: BaseUI, onJobsListener: OnJobsListener) {
        val jobs = MutableLiveData<List<Job>>()
        disposable = Api.getJobs().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe(
                        {
                            jobs.postValue(it)
                            jobs.observe(baseUI, Observer {
                                Log.i("OrdersAPI", Api.gson.toJson(it))
                                onJobsListener.onJobsListed(it)
                            })
                        }, {
                    Log.e("OrdersAPI", it.toString())
                    onJobsListener.onApiError(it.toString())

                }
                )


    }

    fun cancelOrder(baseUI: BaseUI, orderID: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.cancelOrder(orderID).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("CancelOrderAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("CancelOrderAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun completeOrder(baseUI: BaseUI, orderID: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.completedOrder(orderID).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("CompleteOrderAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("CompleteOrderAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun rateProvider(baseUI: BaseUI, pid: String, rating: Float, review: String, onServiceUpdateListener: OnServiceUpdateListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.rateProvider(pid, rating, review).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(baseUI, Observer {
                                Log.i("RatingAPI", Api.gson.toJson(it))
                                onServiceUpdateListener.onServiceUpdated(it!!.status)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("RatingAPI", it.toString())
                    onServiceUpdateListener.onApiError(it.toString())
                }
                )
    }

    fun availDiscount(base: Base, pid: String, code: String, onAPIListener: OnAPIListener) {
        val data = MutableLiveData<ApiResponse>()
        disposable = Api.availDiscount(pid, code).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer { discount ->
                                Log.i("AvailDiscountAPI", Api.gson.toJson(discount))
                                onAPIListener.onApiResponse(discount!!)
                                disposable.dispose()
                            })
                        }, {
                    Log.e("AvailDiscountAPI", it.toString())
                    onAPIListener.onApiError(it.toString())
                }
                )
    }

    fun getFortToken(base: Base, params: HashMap<String, String>, onFortListener: OnFortListener) {
        val data = MutableLiveData<FortResponse>()
        disposable = Api.getFortSDKToken(params).subscribeOn(Schedulers.io())
                .observeOn(mainThread()).subscribe(
                        {
                            data.postValue(it)
                            data.observe(base, Observer { fortResponse ->
                                onFortListener.onFortResponse(fortResponse!!)
                                disposable.dispose()
                            })
                        }, {
                    it.printStackTrace()
                    onFortListener.onApiError(it.toString())
                }
                )
    }

    fun getPromotions(baseUI: BaseUI, onApiListener: OnAPIListener) {
        val services = MutableLiveData<ApiResponse>()
        disposable = Api.getPromotions().subscribeOn(Schedulers.io())
                .observeOn(mainThread())
                .subscribe({ apiResponse ->
                    services.postValue(apiResponse)
                    services.observe(baseUI, Observer {
                        onApiListener.onApiResponse(it!!)
                        disposable.dispose()
                    })

                }, {
                    onApiListener.onApiError(it.toString())
                }
                )


    }

    fun getPublicIP(base: Base, onIpv4Listener: OnIpv4Listener) {
        val data = MutableLiveData<ResponseBody>()
        disposable = Api.getPublicIpv4().subscribeOn(Schedulers.io()).observeOn(mainThread()).subscribe({
            data.postValue(it)
            data.observe(base, Observer { rb ->
                onIpv4Listener.onIPv4(rb!!.string().replace("\n", ""))
                disposable.dispose()
            })
        }, {
            it.printStackTrace()
        })


    }


}