package mobin.beautyclickcustomer.adapters

import android.support.v4.app.FragmentPagerAdapter
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base
import mobin.beautyclickcustomer.subcontrollers.BaseUI
import mobin.beautyclickcustomer.subcontrollers.ProviderFreelancer
import mobin.beautyclickcustomer.subcontrollers.ProviderStore

class ProvidersAdapter(base: Base, private val catID: Int) : FragmentPagerAdapter(base.supportFragmentManager) {

    private val title: List<String> = listOf(base.getString(R.string.store), base.getString(R.string.freelancer))

    override fun getItem(position: Int): BaseUI {
        return when (position) {
            0 -> {
                ProviderStore.newInstance(catID)
            }
            else -> {
                ProviderFreelancer.newInstance(catID)
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }


}