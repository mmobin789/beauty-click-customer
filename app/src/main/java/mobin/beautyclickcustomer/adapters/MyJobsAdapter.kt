package mobin.beautyclickcustomer.adapters

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import kotlinx.android.synthetic.main.rate_review_dialog.*
import kotlinx.android.synthetic.main.scheduled_job_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base.Companion.beautyClickImpl
import mobin.beautyclickcustomer.controllers.Base.Companion.dismissProgressBar
import mobin.beautyclickcustomer.controllers.Base.Companion.showProgressBar
import mobin.beautyclickcustomer.listeners.OnServiceUpdateListener
import mobin.beautyclickcustomer.models.Job
import mobin.beautyclickcustomer.subcontrollers.MyJobs


class MyJobsAdapter(private val myJobs: MyJobs, private val list: MutableList<Job>) : RecyclerView.Adapter<ViewHolder>(), OnServiceUpdateListener {
    private var position = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.scheduled_job_row, parent, false))

        vh.completedTV.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            showProgressBar()
            beautyClickImpl.completeOrder(myJobs, order.orderId, object : OnServiceUpdateListener {
                override fun onServiceUpdated(isUpdated: Boolean) {
                    dismissProgressBar()
                    val result = if (isUpdated) {
                        removeJob()
                        showRatingsUI(order.userId, it.context)
                        R.string.success
                    } else R.string.failed
                    Toast.makeText(myJobs.context, result, Toast.LENGTH_SHORT).show()
                }

                override fun onApiError(error: String) {
                    dismissProgressBar()
                    Toast.makeText(myJobs.context, error, Toast.LENGTH_SHORT).show()

                }
            })
        }
        vh.cancel.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            showProgressBar()
            beautyClickImpl.cancelOrder(myJobs, order.orderId, this)
        }
        vh.viewMap.setOnClickListener {
            position = vh.adapterPosition
            val order = list[position]
            if (order.lat != null && order.lng != null)
                openMapApp(order.lat, order.lng)
            else Toast.makeText(it.context, R.string.not_found, Toast.LENGTH_SHORT).show()
        }
        return vh
    }

//
//    private fun setUserType(vh: ViewHolder) {
//        if (list[vh.adapterPosition].userType.equals("freelancer",true)) {
//            vh.label.visibility = View.GONE
//            vh.date.visibility = View.GONE
//        }
//    }

    override fun getItemCount(): Int {
        return list.size
    }

    fun removeJob() {
        list.removeAt(position)
        notifyItemRemoved(position)
        if (itemCount == 0)
            myJobs.showListErrorUI(R.string.not_found)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = list[position]
        holder.job.text = job.service
        holder.price.text = job.price
        holder.date.text = job.jobPostDate
        holder.customerName.text = job.customer
        holder.loc.text = job.city
        //setUserType(holder)


    }

    private fun openMapApp(lat: Double, lng: Double) {
        // Create a Uri from an intent string. Use the result to create an Intent.
        val gmmIntentUri = Uri.parse("geo:$lat,$lng?z=12&q=$lat,$lng(${myJobs.getString(R.string.b_a)})")

// Create an Intent from gmmIntentUri. Set the action to ACTION_VIEW
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
// Make the Intent explicit by setting the Google Maps package
        mapIntent.setPackage("com.google.android.apps.maps")

        if (mapIntent.resolveActivity(myJobs.context!!.packageManager) != null)
        // Attempt to start an activity that can handle the Intent
            myJobs.startActivity(mapIntent)
        else Toast.makeText(myJobs.context, R.string.d_map, Toast.LENGTH_SHORT).show()
    }

    override fun onServiceUpdated(isUpdated: Boolean) {
        dismissProgressBar()
        Toast.makeText(myJobs.context, if (isUpdated) {
            R.string.success
        } else R.string.failed, Toast.LENGTH_SHORT).show()

        if (isUpdated)
            removeJob()
    }

    override fun onApiError(error: String) {
        dismissProgressBar()
        Toast.makeText(myJobs.context, error, Toast.LENGTH_SHORT).show()
    }

    private fun showRatingsUI(pid: String, context: Context) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.rate_review_dialog)
        dialog.show()
        dialog.ok.setOnClickListener {
            val rating = dialog.ratingBar.rating
            val review = dialog.etReview.text.toString()
            if (rating >= 1) {
                showProgressBar()
                beautyClickImpl.rateProvider(myJobs, pid, rating, review, object : OnServiceUpdateListener {
                    override fun onServiceUpdated(isUpdated: Boolean) {
                        dismissProgressBar()
                        val result = if (isUpdated) {
                            dialog.dismiss()
                            R.string.success
                        } else R.string.failed
                        Toast.makeText(it.context, result, Toast.LENGTH_SHORT).show()
                    }

                    override fun onApiError(error: String) {
                        dismissProgressBar()
                        Toast.makeText(it.context, error, Toast.LENGTH_SHORT).show()
                    }
                })
            } else Toast.makeText(it.context, R.string.rating, Toast.LENGTH_SHORT).show()

        }
    }


}