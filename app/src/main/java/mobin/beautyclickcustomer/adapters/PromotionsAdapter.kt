package mobin.beautyclickcustomer.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.promo_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Booking
import mobin.beautyclickcustomer.models.Promotion
import mobin.beautyclickcustomer.models.ProviderService

class PromotionsAdapter(private val list: List<Promotion>) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.promo_row, parent, false))
        vh.bookBtn.setOnClickListener {
            val promotion = list[vh.adapterPosition]
            val services = getProviderServices(promotion.services!!)
            val booking = Intent(it.context, Booking::class.java)
                    .putExtra("promo", true)
                    .putExtra("isFreelancer", promotion.provider == "freelancer")
                    .putParcelableArrayListExtra("promotions", services)
                    .putExtra("pid", promotion.pid.toString())

            val latLng = if (promotion.lat == null || promotion.lng == null)
                LatLng(-1.0, -1.0)
            else LatLng(promotion.lat, promotion.lng)

            booking.putExtra("storeLatLng", latLng)

            it.context.startActivity(booking)


        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val promotion = list[position]

        holder.promoTV.text = if (promotion.title.isNullOrBlank())
            holder.containerView.context.getString(R.string.not_found)
        else promotion.title


        if (promotion.services == null || promotion.services.isEmpty()) {

            holder.bookBtn.visibility = View.GONE
        }


    }

    private fun getProviderServices(services: List<Promotion.Service>): ArrayList<ProviderService.Data> {
        val promotionServices = arrayListOf<ProviderService.Data>()
        services.distinctBy {
            promotionServices.add(ProviderService.Data(it.serviceName, it.id.toString(), false, it.price))

        }

        return promotionServices
    }
}