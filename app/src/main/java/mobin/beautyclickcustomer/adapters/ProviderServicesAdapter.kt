package mobin.beautyclickcustomer.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.services_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.listeners.OnServiceClickListener
import mobin.beautyclickcustomer.models.ProviderService


class ProviderServicesAdapter(private val list: List<ProviderService.Data>, private val discountPercentage: Float) : RecyclerView.Adapter<ViewHolder>() {

    lateinit var onServiceClickListener: OnServiceClickListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.services_row, parent, false))
        vh.checkbox.setOnClickListener {
            val position = vh.adapterPosition
            if (vh.checkbox.isChecked)
                onServiceClickListener.onListItemClicked(vh)
            else onServiceClickListener.onServiceUnSelected(position)
        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val service = list[position]
        val s = service.service.trim() + " " + holder.containerView.context.getString(R.string.price) + " " + getBeautyClickPromotionPrice(service.price)
        holder.serviceTV.text = s
    }

    private fun getBeautyClickPromotionPrice(price: Float): Float {
        var discountedPrice = price
        if (price > 0 && discountPercentage > 0) {
            discountedPrice = (price / 100 * discountPercentage)

        }
        return discountedPrice
    }


}