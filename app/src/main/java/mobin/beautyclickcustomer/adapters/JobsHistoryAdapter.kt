package mobin.beautyclickcustomer.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.job_history_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.models.Job


class JobsHistoryAdapter(private val list: List<Job>) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.job_history_row, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val job = list[position]


        val res = if (job.status.equals("Completed", true)) {
            R.drawable.checked_circle

        } else {

            R.drawable.cancelled

        }
        holder.statusIV.setImageResource(res)
        holder.statusTV.text = job.status
        holder.job.text = job.service
        holder.price.text = job.price
        holder.date.text = job.jobPostDate
        holder.customerName.text = job.customer
        holder.ratingBar.rating = job.jobRating

    }

}