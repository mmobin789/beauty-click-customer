package mobin.beautyclickcustomer.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.provider_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base.Companion.loadWithGlide
import mobin.beautyclickcustomer.controllers.NewJob
import mobin.beautyclickcustomer.listeners.OnListItemClickListener
import mobin.beautyclickcustomer.models.Store
import mobin.beautyclickcustomer.models.WorkingHours


class StoresAdapter(private val newJob: NewJob, private val promo: Boolean, private val service: String, private val list: List<Store.Employee>) : RecyclerView.Adapter<ViewHolder>() {
    lateinit var onListItemClickListener: OnListItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.provider_row, parent, false))
        if (promo) {
            vh.discountBtn.visibility = View.VISIBLE
            //    vh.bookBtn.visibility = View.GONE
        }
        vh.bookBtn.setOnClickListener {
            onListItemClickListener.onListItemClicked(vh)
        }
        vh.discountBtn.setOnClickListener {
            val pid = list[vh.adapterPosition].id
            if (pid.isNullOrBlank())
                Toast.makeText(newJob, R.string.not_found, Toast.LENGTH_SHORT).show()
            else
                newJob.promoDialog(pid!!)
        }
        return vh
    }

    override fun getItemCount(): Int {
        return list.size
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val employee = list[position]
        holder.name.text = employee.name
        holder.service.text = service
        holder.price.text = employee.minPrice
        val dailyTimings = employee.dailyTimings

        if (dailyTimings != null && dailyTimings.isNotEmpty()) {
            setDaySpinner(holder.spinnerDays, dailyTimings, holder.timeTV)
        } else {
            holder.spinnerUI.visibility = View.GONE
            holder.timeTV.setText(R.string.not_found)

        }
        //   val phone = "${holder.containerView.context.getString(R.string.phoneNo)} ${employee.mobile}"
        if (employee.image != null)
            loadWithGlide(employee.image, holder.img, true)
    }

    private fun getWorkingHours(dailyTimings: List<String>): WorkingHours {
        val days = mutableListOf<String>()
        val timeSlots = mutableListOf<String>()
        dailyTimings.distinctBy {

            val day = it.split("_")[0]
            val timeSlot = it.split("_")[1]
            days.add(day)
            timeSlots.add(timeSlot)

        }




        return WorkingHours(days, timeSlots)

    }

    private fun setDaySpinner(spinner: Spinner, dailyTimings: List<String>, displayTV: TextView) {

        val workingHours = getWorkingHours(dailyTimings)
        spinner.adapter = ArrayAdapter<String>(spinner.context, android.R.layout.simple_spinner_dropdown_item, workingHours.days)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {


                displayTV.text = workingHours.timeSlot[position]

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

}