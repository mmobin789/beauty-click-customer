package mobin.beautyclickcustomer.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import mobin.beautyclickcustomer.subcontrollers.BaseUI
import mobin.beautyclickcustomer.subcontrollers.ChooseLanguage
import mobin.beautyclickcustomer.subcontrollers.SignUpForm

class SignUpPager(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): BaseUI {

        return when (position) {

            1 -> {
                SignUpForm.newInstance()
            }
            else -> {
                ChooseLanguage.newInstance()

            }
        }

    }

    override fun getCount(): Int {
        return 2
    }

}