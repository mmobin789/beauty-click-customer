package mobin.beautyclickcustomer.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.provider_row.*
import mobin.beautyclickcustomer.R
import mobin.beautyclickcustomer.controllers.Base.Companion.loadWithGlide
import mobin.beautyclickcustomer.controllers.NewJob
import mobin.beautyclickcustomer.listeners.OnListItemClickListener
import mobin.beautyclickcustomer.models.Freelancer

class FreelancersAdapter(private val newJob: NewJob, private val promo: Boolean, private val service: String, private val list: List<Freelancer.Data>) : RecyclerView.Adapter<ViewHolder>() {
    lateinit var onListItemClickListener: OnListItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val vh = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.provider_row, parent, false))
        vh.spinnerUI.visibility = View.GONE
        if (promo)
            vh.discountBtn.visibility = View.VISIBLE
        vh.bookBtn.setOnClickListener {
            onListItemClickListener.onListItemClicked(vh)
        }
        vh.discountBtn.setOnClickListener {
            val pid = list[vh.adapterPosition].id
            if (pid.isNullOrBlank())
                Toast.makeText(newJob, R.string.not_found, Toast.LENGTH_SHORT).show()
            else
                newJob.promoDialog(pid!!)
        }
        return vh

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val freelancer = list[position]
        holder.name.text = freelancer.name
        holder.service.text = service
        holder.price.text = freelancer.minPrice
        val phone = "${holder.containerView.context.getString(R.string.phoneNo)} ${freelancer.mobile}"
        holder.timeTV.text = phone
        if (freelancer.image != null)
            loadWithGlide(freelancer.image, holder.img, true)

    }

}